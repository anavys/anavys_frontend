# Application Desing Doc

## Previous Work

I tried out and failed with react-admin. The positive thing about it was how it was build the design pattern for the API, which was an adapter pattern using:
 - `convertDataRequestToHttp`
 - `convertHttpResponse`
 - `convertHttpResponse`

all wrapped in a `function dataProvider(api_url, httpClient)` that return this at the end:

```javascript

// The action types for the api, UPDATE_MANY, DELETE_MANY and UPDATE_MANY are not availabe per default
TYPES = {
    GET_LIST,
    GET_ONE,
    CREATE,
    UPDATE,
    UPDATE_MANY,
    DELETE,
    DELETE_MANY,
    GET_MANY,
    GET_MANY_REFERENCE,
}

const { url, options } = convertDataRequestToHttp(type, resource, params);
        return myHttpClient(url, options)
            .then(response =>  {
                return convertHttpResponse(response, type, resource, params)
            });
```
where 