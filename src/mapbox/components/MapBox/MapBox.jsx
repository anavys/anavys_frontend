import React from 'react';
import ReactMapGL from 'react-map-gl';

// mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}

const MapBox = props => {
    const {width, height, ...rest} = props.viewport;
    return (
            <ReactMapGL
                width={props.width || '100%' }
                height={props.height || 400}
                {...rest}
                onViewportChange={viewport => props.onViewportChange(viewport)}

                mapboxApiAccessToken='pk.eyJ1IjoiZGFkYWRhbW90aGEiLCJhIjoiY2p1cjN5eWV5MDIwdDN5azM1bGZ5N3ExMCJ9.mbo0RQp7F2hZ1gNfODsWww'
            >
                {props.children}
            </ReactMapGL>
    );
};

export default MapBox;