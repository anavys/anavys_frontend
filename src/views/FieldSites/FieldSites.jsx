import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Resource from '../../drf/components/resource';
import DrfList from '../../drf/components/DrfList';
import DrfEdit from "../../drf/components/DrfEdit";
import DrfDetail from "../../drf/components/DrfDetail";
import DrfCreate from "../../drf/components/DrfCreate";
import LocationOn from '@material-ui/icons/LocationOn';

import {
    FIELD_TEXT,
    INPUT_SELECT,
    INPUT_TEXT,
    INPUT_TEXT_AREA,
    FIELD_TEXT_AREA,
    INPUT_LOCATION,
    ON_OVERVIEW_MAP,
    SVG_DOT,
    FIELD_FOREIGN_KEY,
    INPUT_FOREIGN_KEY,
    ICON_MARKER
} from "../../drf/constants";

const RESOURCE = 'field-sites';

const FIELDSITE_TYPES = [

    {value: 'RIVGAUG', label: 'River gauging station'},
    {value: 'PIEZO', label: 'Piezometer'},
    {value: 'BH', label: 'Borehole'},

    {value: 'OUTWELL', label: 'Pumping well'},
    {value: 'INWELL', label: 'Injection well'},
    {value: 'WELL', label: 'Well'},

    {value: 'WEAST', label: 'Weather station'},
    {value: 'RAINGAUG', label: 'Rain gauging station'},

    {value: 'OPENFIELD', label: 'Open field site'},
    {value: 'CONSTR', label: 'Construction Site'},
    {value: 'DAM', label: 'Dam'},

    {value: 'GLACE', label: 'Glacier'},
    {value: 'AQUIT', label: 'Aquitard'},
    {value: 'AQUIC', label: 'Aquiclud'},
    {value: 'KARST', label: 'Karst Aquifer'}

];

const listConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT
    },
    {
        field: 'project',
        fieldName: 'Related Project',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'projects',
        relatedDisplayField:  'name'
    },
    {
        field: 'coordinates',
        type: ON_OVERVIEW_MAP,
        markerType: ICON_MARKER,
        color: 'blue',
        radius: 6,
    },
];

const editConfig = [
    {
        field: 'type',
        fieldName: 'Type',
        type: INPUT_SELECT,
        options: FIELDSITE_TYPES
    },
    {
        field: 'project',
        fieldName: 'Related Project',
        relatesTo: 'projects',
        relatedDisplayField: 'name',
        type: INPUT_FOREIGN_KEY
    },
    {
        field: 'name',
        fieldName: 'Name',
        type: INPUT_TEXT
    },
    {
        field: 'description',
        fieldName: 'Description',
        type: INPUT_TEXT_AREA
    },
    {
        field: 'coordinates',
        fieldName: 'Coordinates',
        type: INPUT_LOCATION
    },
];

const detailConfig = [
    {
        field: 'type',
        fieldName: 'Type',
        type: FIELD_TEXT
    },
    {
        field: 'project',
        fieldName: 'Related Project',
        type: FIELD_TEXT
    },
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT
    },
    {
        field: 'description',
        fieldName: 'Description',
        type: FIELD_TEXT_AREA
    },

];

const List = props => {

    return (
        <DrfList
            url={RESOURCE}
            listName='Field Sites'
            detailName='Field Site'
            headerColor='rose'
            buttonColor='primary'
            config={listConfig}
            {...props} />
    )
};

const Edit = props => {

    return (
        <DrfEdit url={RESOURCE}
                 listName='Field Sites'
                 detailName='Field Site'
                 headerColor='primary'
                 buttonColor='rose'
                 config={editConfig} {...props} />
    )
};

const Detail = props => {
    return (
        <DrfDetail url={RESOURCE}
                   listName='Field Sites'
                   detailName='Field Site'
                   headerColor='primary'
                   buttonColor='primary'
                   config={detailConfig} {...props} />
    )
};


const Create = props => {
    return (
        <DrfCreate url={RESOURCE}
                   listName='Field Sites'
                   detailName='Field Site'
                   headerColor='primary'
                   buttonColor='primary'
                   config={editConfig} {...props} />
    )
};

const FieldSites = props => {
    return (
        <div>
            <Resource
                url={RESOURCE}
                list={List}
                edit={Edit}
                detail={Detail}
                create={Create}
                listName='Field Sites'
                detailName='Field Site'
                {...props}
            />

        </div>
    )

};

FieldSites.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(FieldSites);

