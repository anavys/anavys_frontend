import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Resource from '../../drf/components/resource';
import DrfList from '../../drf/components/DrfList';
import DrfEdit from "../../drf/components/DrfEdit";
import DrfDetail from "../../drf/components/DrfDetail";
import DrfCreate from "../../drf/components/DrfCreate";

import {
    FIELD_TEXT,
    INPUT_TEXT,
    FIELD_BOOL,
    INPUT_BOOL,
    ON_OVERVIEW_MAP,
    SVG_DOT,
    INPUT_LOCATION,
    FIELD_FOREIGN_KEY,
    INPUT_FOREIGN_KEY
} from "../../drf/constants";

const RESOURCE = 'field-devices';
const LIST_NAME = 'Field Devices';
const DETAIL_NAME = 'Field Device';

const listConfig = [
    {
        field: 'active',
        fieldName: 'Active',
        type: FIELD_BOOL
    },
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT
    },
    {
        field: 'project',
        fieldName: 'Related Project',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'projects',
        relatedDisplayField: 'name'
    },
    {

        field: 'model',
        fieldName: 'Device Model',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'device-models',
        relatedDisplayField: 'name',

    },
    {
        field: 'coordinates',
        type: ON_OVERVIEW_MAP,
        markerType: SVG_DOT,
        color: 'orange',
        radius: 6,
    },

];

const editConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: INPUT_TEXT,
    },
    {
        field: 'project',
        fieldName: 'Project',
        type: INPUT_FOREIGN_KEY,
        relatesTo: 'projects',
        relatedDisplayField: 'name',
    },
    {
        field: 'model',
        fieldName: 'Model',
        type: INPUT_FOREIGN_KEY,
        relatesTo: 'device-models',
        relatedDisplayField: 'name',

    },
    {
        field: 'active',
        fieldName: 'Active',
        type: INPUT_BOOL
    },
    {
        field: 'serialNo',
        fieldName: 'Serial Number',
        type: INPUT_TEXT
    },
    {
        field: 'coordinates',
        fieldName: 'Coordinates',
        type: INPUT_LOCATION,
    },
];


const detailConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT,
    },

    {
        field: 'Project',
        fieldName: 'project',
        type: FIELD_TEXT,
        options: null
    },
    {
        field: 'Model',
        fieldName: 'model',
        type: FIELD_TEXT,
        options: null
    },
    {
        field: 'active',
        fieldName: 'Active',
        type: FIELD_BOOL
    },
    {
        field: 'serialNo',
        fieldName: 'Serial Number',
        type: FIELD_TEXT
    },
    {
        field: 'coordinates',
        type: ON_OVERVIEW_MAP,
        markerType: SVG_DOT,
        color: 'red',
        radius: 6,
    },

];

const List = props => {

    return (
        <DrfList
            url='field-devices'
            listName={LIST_NAME}
            detailName={DETAIL_NAME}
            headerColor='warning'
            buttonColor='primary'
            config={listConfig}
            {...props} />
    )

};

const Edit = props => {

    return (
        <DrfEdit url={RESOURCE}
                 listName={LIST_NAME}
                 detailName={DETAIL_NAME}
                 headerColor='warning'
                 buttonColor='primary'
                 config={editConfig} {...props} />
    )
};


const Detail = props => {
    return (
        <DrfDetail url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='warning'
                   buttonColor='primary'
                   config={detailConfig} {...props} />
    )
};


const Create = props => {
    return (
        <DrfCreate url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='warning'
                   buttonColor='primary'
                   config={editConfig} {...props} />
    )
};

const FieldDevices = props => {
    return (
        <Resource
            url={RESOURCE}
            list={List}
            edit={Edit}
            detail={Detail}
            create={Create}
            listName={LIST_NAME}
            detailName={DETAIL_NAME}
            {...props}
        />)
};

FieldDevices.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(FieldDevices);

