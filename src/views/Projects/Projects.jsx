import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Resource from '../../drf/components/resource';
import DrfList from '../../drf/components/DrfList';
import DrfEdit from "../../drf/components/DrfEdit";
import DrfDetail from "../../drf/components/DrfDetail";
import DrfCreate from "../../drf/components/DrfCreate";

import {
    FIELD_TEXT,
    FIELD_BOOL,
    FIELD_DATE,
    INPUT_TEXT,
    INPUT_DATE,
    INPUT_BOOL,
    INPUT_TEXT_AREA, FIELD_TEXT_AREA
} from "../../drf/constants";
import MapBoxModal from "../../drf/components/MapBoxModal";

const listConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT
    },
    {
        field: 'active',
        fieldName: 'Active',
        type: FIELD_BOOL
    },
    {
        field: 'startDate',
        fieldName: 'Start',
        type: FIELD_DATE,
    },
    {
        field: 'stopDate',
        fieldName: 'Stops at',
        type: FIELD_DATE
    },
    // {
    //     field: 'createdAt',
    //     fieldName: 'Created',
    //     type: FIELD_DATE
    // }
];

const ProjectList = props => {

    return (
        <DrfList
            url='projects'
            listName='Projects'
            detailName='Project'
            headerColor='primary'
            buttonColor='rose'
            config={listConfig}
            {...props} />
    )

};

const editConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: INPUT_TEXT
    },
    {
        field: 'description',
        fieldName: 'Description',
        type: INPUT_TEXT_AREA
    },
    {
        field: 'active',
        fieldName: 'Active',
        type: INPUT_BOOL
    },
    {
        field: 'startDate',
        fieldName: 'Start',
        type: INPUT_DATE
    },
    {
        field: 'stopDate',
        fieldName: 'Stops at',
        type: INPUT_DATE
    }
]

const ProjectEdit = props => {

    return (
        <DrfEdit url='projects'
                 listName='Projects'
                 detailName='Project'
                 headerColor='primary'
                 buttonColor='rose'
                 config={editConfig} {...props} />
    )

};

const detailConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT
    },
    {
        field: 'description',
        fieldName: 'Description',
        type: FIELD_TEXT_AREA
    },
    {
        field: 'active',
        fieldName: 'Active',
        type: FIELD_BOOL
    },
    {
        field: 'startDate',
        fieldName: 'Start',
        type: FIELD_DATE
    },
    {
        field: 'stopDate',
        fieldName: 'Stops at',
        type: FIELD_DATE
    }
];

const ProjectDetail = props => {
    return (
        <DrfDetail url='projects'
                   listName='Projects'
                   detailName='Project'
                   headerColor='primary'
                   buttonColor='primary'
                   config={detailConfig} {...props} />
    )
};


const ProjectCreate = props => {
    return (
        <DrfCreate url='projects'
                   listName='Projects'
                   detailName='Project'
                   headerColor='primary'
                   buttonColor='primary'
                   config={editConfig} {...props} />
    )
};

const Projects = props => {
    return (
        <div>
            <Resource
                url='projects'
                list={ProjectList}
                edit={ProjectEdit}
                detail={ProjectDetail}
                create={ProjectCreate}
                listName='Projects'
                detailName='Project'
                {...props}
            />
        </div>
    )
};

Projects.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Projects);

