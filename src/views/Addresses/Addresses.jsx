import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Resource from '../../drf/components/resource';
import DrfList from '../../drf/components/DrfList';
import DrfEdit from "../../drf/components/DrfEdit";
import DrfDetail from "../../drf/components/DrfDetail";
import DrfCreate from "../../drf/components/DrfCreate";

import {
    FIELD_TEXT,
    INPUT_TEXT,

} from "../../drf/constants";

const RESOURCE = 'addresses';
const LIST_NAME = 'Addresses';
const DETAIL_NAME = 'Address';

const listConfig = [
    {
        field: 'street',
        fieldName: 'Street',
        type: FIELD_TEXT
    },
    {
        field: 'streetNumber',
        fieldName: 'Street Number',
        type: FIELD_TEXT
    },

    {
        field: 'city',
        fieldName: 'City',
        type: FIELD_TEXT
    }
];


const editConfig = [
    {
        field: 'street',
        fieldName: 'Street',
        type: INPUT_TEXT
    },
    {
        field: 'streetNumber',
        fieldName: 'Street Number',
        type: INPUT_TEXT
    },

    {
        field: 'postfach',
        fieldName: 'Postal Box',
        type: INPUT_TEXT
    },
    {
        field: 'plz',
        fieldName: 'PLZ / ZIP',
        type: INPUT_TEXT
    },
    {
        field: 'city',
        fieldName: 'City',
        type: INPUT_TEXT
    },
    {
        field: 'canton',
        fieldName: 'Canton',
        type: INPUT_TEXT
    }
];

const detailConfig = [
    {
        field: 'street',
        fieldName: 'Street',
        type: FIELD_TEXT
    },
    {
        field: 'streetNumber',
        fieldName: 'Street Number',
        type: FIELD_TEXT
    },

    {
        field: 'postfach',
        fieldName: 'Postal Box',
        type: FIELD_TEXT
    },
    {
        field: 'plz',
        fieldName: 'PLZ / ZIP',
        type: FIELD_TEXT
    },
    {
        field: 'city',
        fieldName: 'City',
        type: FIELD_TEXT
    },
    {
        field: 'canton',
        fieldName: 'Canton',
        type: FIELD_TEXT
    }
];

const AddressList = props => {

    return (
        <DrfList
            url={RESOURCE}
            listName={LIST_NAME}
            detailName={DETAIL_NAME}
            headerColor='primary'
            buttonColor='rose'
            config={listConfig}
            {...props} />
    )
};

const AddressEdit = props => {

    return (
        <DrfEdit url={RESOURCE}
                 listName={LIST_NAME}
                 detailName={DETAIL_NAME}
                 headerColor='primary'
                 buttonColor='rose'
                 config={editConfig} {...props} />
    )

};


const AddressDetail = props => {
    return (
        <DrfDetail url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='primary'
                   buttonColor='primary'
                   config={detailConfig} {...props} />
    )
};


const AddressCreate = props => {
    return (
        <DrfCreate url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='primary'
                   buttonColor='primary'
                   config={editConfig} {...props} />
    )
};

const Addresses = props => {
    return (
        <Resource
            url={LIST_NAME}
            list={AddressList}
            edit={AddressEdit}
            detail={AddressDetail}
            create={AddressCreate}
            listName={LIST_NAME}
            detailName={DETAIL_NAME}
            {...props}
        />)
};

Addresses.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Addresses);
// export default Addresses;

