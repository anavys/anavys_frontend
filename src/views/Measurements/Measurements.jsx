import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Resource from '../../drf/components/resource';
import DrfList from '../../drf/components/DrfList';
import DrfEdit from "../../drf/components/DrfEdit";
import DrfDetail from "../../drf/components/DrfDetail";
import DrfCreate from "../../drf/components/DrfCreate";

import {
    FIELD_TEXT,
    INPUT_TEXT,
    FIELD_FOREIGN_KEY,
    INPUT_FOREIGN_KEY, FIELD_DATE
} from "../../drf/constants";

const RESOURCE = 'measurements';
const LIST_NAME = 'Measurements';
const DETAIL_NAME = 'Measurement';

const listConfig = [
    {
        field: 'site',
        fieldName: 'Related Site',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'field-sites',
        relatedDisplayField:  'name'
    },
    {
        field: 'device',
        fieldName: 'Related Device',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'field-devices',
        relatedDisplayField:  'name'
    },
    {
        field: 'project',
        fieldName: 'Related Project',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'projects',
        relatedDisplayField:  'name'
    },
     {
        field: 'value',
        fieldName: 'Value',
        type: FIELD_TEXT,
    },
    {
        field: 'unit',
        fieldName: 'Unit',
        type: FIELD_TEXT,
    },
    {
        field: 'createdAt',
        fieldName: 'Created',
        type: FIELD_DATE
    }

];

const editConfig = [
    {
        field: 'site',
        fieldName: 'Related Site',
        type: INPUT_FOREIGN_KEY,
        relatesTo: 'field-sites',
        relatedDisplayField:  'name'
    },
    {
        field: 'device',
        fieldName: 'Related Device',
        type: INPUT_FOREIGN_KEY,
        relatesTo: 'field-devices',
        relatedDisplayField:  'name'
    },
    {
        field: 'project',
        fieldName: 'Related Project',
        type: INPUT_FOREIGN_KEY,
        relatesTo: 'projects',
        relatedDisplayField:  'name'
    },
     {
        field: 'value',
        fieldName: 'Value',
        type: INPUT_TEXT,
    },
    {
        field: 'unit',
        fieldName: 'Unit',
        type: INPUT_TEXT,
    },
];


const List = props => {

    return (
        <DrfList
            url={RESOURCE}
            listName={LIST_NAME}
            detailName={DETAIL_NAME}
            headerColor='primary'
            buttonColor='success'
            config={listConfig}
            {...props} />
    )
};

const Edit = props => {

    return (
        <DrfEdit url={RESOURCE}
                 listName={LIST_NAME}
                 detailName={DETAIL_NAME}
                 headerColor='primary'
                 buttonColor='success'
                 config={editConfig} {...props} />
    )
};

const Detail = props => {
    return (
        <DrfDetail url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='primary'
                   buttonColor='success'
                   config={listConfig} {...props} />
    )
};


const Create = props => {
    return (
        <DrfCreate url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='primary'
                   buttonColor='success'
                   config={editConfig} {...props} />
    )
};

const Measurements = props => {
    return (
        <div>
            <Resource
                url={RESOURCE}
                list={List}
                edit={Edit}
                detail={Detail}
                create={Create}
                listName={LIST_NAME}
                detailName={DETAIL_NAME}
                {...props}
            />

        </div>
    )

};

Measurements.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Measurements);

