import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Resource from '../../drf/components/resource';
import DrfList from '../../drf/components/DrfList';
import DrfEdit from "../../drf/components/DrfEdit";
import DrfDetail from "../../drf/components/DrfDetail";
import DrfCreate from "../../drf/components/DrfCreate";

import {
    FIELD_TEXT, FIELD_TEXT_AREA, INPUT_TEXT, INPUT_TEXT_AREA,
} from "../../drf/constants";


const RESOURCE = 'device-models';
const LIST_NAME = 'Device Models';
const DETAIL_NAME = 'Device Model';

const listConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT
    },
    {
        field: 'description',
        fieldName: 'Description',
        type: FIELD_TEXT_AREA,
    },
    {
        field: 'manufacturor',
        fieldName: 'Manufacturor',
        type: FIELD_TEXT,
    },
];


const editConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: INPUT_TEXT
    },
    {
        field: 'description',
        fieldName: 'Description',
        type: INPUT_TEXT_AREA,
    },
    {
        field: 'manufacturor',
        fieldName: 'Manufacturor',
        type: INPUT_TEXT,
    },
    {
        field: 'sampleFileName',
        fieldName: 'Sample File Name',
        type: INPUT_TEXT
    },
    {
        field: 'dtFormat',
        fieldName: 'Date Time Format',
        type: INPUT_TEXT
    },
];

const detailConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT
    },
    {
        field: 'description',
        fieldName: 'Description',
        type: FIELD_TEXT_AREA,
    },
    {
        field: 'manufacturor',
        fieldName: 'Manufacturor',
        type: FIELD_TEXT,
    },
    {
        field: 'sampleFileName',
        fieldName: 'Sample File Name',
        type: FIELD_TEXT
    },
    {
        field: 'dtFormat',
        fieldName: 'Date Time Format',
        type: FIELD_TEXT
    }
];

const Edit = props => {

    return (
        <DrfEdit url={RESOURCE}
                 listName={LIST_NAME}
                 detailName={DETAIL_NAME}
                 headerColor='warning'
                 buttonColor='primary'
                 config={editConfig} {...props} />
    )

};

const List = props => {
    return (
        <DrfList
            url={RESOURCE}
            listName={LIST_NAME}
            detailName={DETAIL_NAME}
            headerColor='warning'
            buttonColor='primary'
            config={listConfig}
            {...props} />
    )
};

const Detail = props => {
    return (
        <DrfDetail url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='warning'
                   buttonColor='primary'
                   config={detailConfig} {...props} />
    )
};


const Create = props => {
    return (
        <DrfCreate url={RESOURCE}
                   listName='Field Devices'
                   detailName='Field Device'
                   headerColor='warning'
                   buttonColor='primary'
                   config={editConfig} {...props} />
    )
};



const DeviceModels = props => {
    return (
        <Resource
            url='field-devices'
            list={List}
            edit={Edit}
            detail={Detail}
            create={Create}
            listName='Field Devices'
            detailName='Field Device'
            {...props}
        />)
};

DeviceModels.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(DeviceModels);

