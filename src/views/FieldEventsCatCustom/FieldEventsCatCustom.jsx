import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Resource from '../../drf/components/resource';
import DrfList from '../../drf/components/DrfList';
import DrfEdit from "../../drf/components/DrfEdit";
import DrfDetail from "../../drf/components/DrfDetail";
import DrfCreate from "../../drf/components/DrfCreate";

import {
    FIELD_TEXT,
    INPUT_TEXT,
} from "../../drf/constants";

const RESOURCE = 'category-custom';
const LIST_NAME = 'Custom Events Categories';
const DETAIL_NAME = 'Custom Event Category';

const listConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT
    },
];

const editConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: INPUT_TEXT,
    },

];

const detailConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT,
    },
];

const List = props => {

    return (
        <DrfList
            url={RESOURCE}
            listName={LIST_NAME}
            detailName={DETAIL_NAME}
            headerColor='rose'
            buttonColor='primary'
            config={listConfig}
            {...props} />
    )
};

const Edit = props => {

    return (
        <DrfEdit url={RESOURCE}
                 listName={LIST_NAME}
                 detailName={DETAIL_NAME}
                 headerColor='primary'
                 buttonColor='rose'
                 config={editConfig} {...props} />
    )
};

const Detail = props => {
    return (
        <DrfDetail url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='primary'
                   buttonColor='primary'
                   config={detailConfig} {...props} />
    )
};


const Create = props => {
    return (
        <DrfCreate url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='primary'
                   buttonColor='primary'
                   config={editConfig} {...props} />
    )
};

const FieldEventsCatCustom = props => {
    return (
        <div>
            <Resource
                url={RESOURCE}
                list={List}
                edit={Edit}
                detail={Detail}
                create={Create}
                listName='Field Sites'
                detailName='Field Site'
                {...props}
            />

        </div>
    )

};

FieldEventsCatCustom.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(FieldEventsCatCustom);

