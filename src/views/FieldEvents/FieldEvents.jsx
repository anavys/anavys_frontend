import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Resource from '../../drf/components/resource';
import DrfList from '../../drf/components/DrfList';
import DrfEdit from "../../drf/components/DrfEdit";
import DrfDetail from "../../drf/components/DrfDetail";
import DrfCreate from "../../drf/components/DrfCreate";

import {
    FIELD_TEXT,
    INPUT_SELECT,
    INPUT_TEXT,
    FIELD_FOREIGN_KEY,
    INPUT_FOREIGN_KEY
} from "../../drf/constants";

const RESOURCE = 'field-events';
const LIST_NAME = 'Field Events';
const DETAIL_NAME = 'Field Event';

const FIELD_EVENT_CATEGORIES = [
    { value: 'chk', label: 'Check'},
    { value: 'c', label: 'Start'},
    { value: 'd', label: 'Stop'},
    { value: 'u', label: 'Change'},
    { value: 'i', label: 'Install'},
    { value: 'r', label: 'Remove'},
    { value: 'dam', label: 'Damaged'},

];


const listConfig = [
    {
        field: 'title',
        fieldName: 'Title',
        type: FIELD_TEXT
    },
    {
        field: 'site',
        fieldName: 'Related Site',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'field-sites',
        relatedDisplayField:  'name'
    },
    {
        field: 'device',
        fieldName: 'Related Device',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'field-devices',
        relatedDisplayField:  'name'
    },

];

const editConfig = [
    {
        field: 'title',
        fieldName: 'Title',
        type: INPUT_TEXT,
    },
    {
        field: 'site',
        fieldName: 'Related Site',
        relatesTo: 'field-sites',
        relatedDisplayField: 'name',
        type: INPUT_FOREIGN_KEY
    },
    {
        field: 'device',
        fieldName: 'Related Device',
        relatesTo: 'field-devices',
        relatedDisplayField: 'name',
        type: INPUT_FOREIGN_KEY
    },
    {
        field: 'category',
        fieldName: 'Default Category',
        type: INPUT_SELECT,
        options: FIELD_EVENT_CATEGORIES
    },
    {
        field: 'categoryCustom',
        fieldName: 'Custom Category',
        type: INPUT_FOREIGN_KEY,
        relatesTo: 'category-custom',
        relatedDisplayField: 'name'
    },
];

const detailConfig = [
    {
        field: 'title',
        fieldName: 'Title',
        type: FIELD_TEXT,
    },
    {
        field: 'site',
        fieldName: 'Related Site',
        relatesTo: 'field-sites',
        relatedDisplayField: 'name',
        type: FIELD_FOREIGN_KEY
    },
    {
        field: 'device',
        fieldName: 'Related Device',
        relatesTo: 'field-devices',
        relatedDisplayField: 'name',
        type: FIELD_FOREIGN_KEY
    },
    {
        field: 'category',
        fieldName: 'Default Category',
        type: FIELD_TEXT,
    },
    {
        field: 'categoryCustom',
        fieldName: 'Custom Category',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'category-custom',
        relatedDisplayField: 'name'
    },
];

const List = props => {

    return (
        <DrfList
            url={RESOURCE}
            listName={LIST_NAME}
            detailName={DETAIL_NAME}
            headerColor='primary'
            buttonColor='success'
            config={listConfig}
            {...props} />
    )
};

const Edit = props => {

    return (
        <DrfEdit url={RESOURCE}
                 listName={LIST_NAME}
                 detailName={DETAIL_NAME}
                 headerColor='primary'
                 buttonColor='success'
                 config={editConfig} {...props} />
    )
};

const Detail = props => {
    return (
        <DrfDetail url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='primary'
                   buttonColor='success'
                   config={detailConfig} {...props} />
    )
};


const Create = props => {
    return (
        <DrfCreate url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='primary'
                   buttonColor='success'
                   config={editConfig} {...props} />
    )
};

const FieldEvents = props => {
    return (
        <div>
            <Resource
                url={RESOURCE}
                list={List}
                edit={Edit}
                detail={Detail}
                create={Create}
                listName={LIST_NAME}
                detailName={DETAIL_NAME}
                {...props}
            />

        </div>
    )

};

FieldEvents.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(FieldEvents);

