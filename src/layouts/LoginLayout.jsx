/* eslint-disable */
import React from 'react';
// import PropTypes from "prop-types";
// import withStyles from "@material-ui/core/styles/withStyles";
// import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
// import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper';
import LoginRegisterTabs from '../components/LoginRegisterTabs'
import backgroundMountain from '../assets/img/photos/leuk_bg.jpg'
import Logo from "../components/Logo";


function TabContainer({ children, dir }) {
    return (
        <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
            {children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        width: 500,
    },
});

const containerStyle = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    // backgroundColor: 'black',
    backgroundImage: `url(${backgroundMountain})`,
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover'
};

const logoStyle = {
    marginBottom: '50px'
};

class LoginRegisterLayout extends React.Component {

    render() {
        return (
            <div id='login-register' style={containerStyle}>
                <div style={logoStyle}>
                    {/*<Logo*/}
                    {/*height='150px'*/}
                    {/*color='light'*/}
                    {/*/>*/}
                    <Logo
                        height='150px'
                        color='light'
                    />
                </div>
                <Paper>
                    <LoginRegisterTabs />
                </Paper>
            </div>
        );
    }
}




export default withRouter(withStyles(styles)(LoginRegisterLayout));