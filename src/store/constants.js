// ACTION TYPES
export const SET_CURRENT_USER = 'setCurrentUser';
export const REMOVE_CURRENT_USER = 'removeCurrentUser';
export const ADD_TOKENS = 'addJWTTokens';
export const REMOVE_TOKENS = 'removeJWTTokens';
export const ADD_TOKENS_TO_STATE = 'addJWTTokensTpState';
export const API = 'api';

// ACTION TYPES for notifcations

export const ADD_NOTIFICATION = 'addNotification';
export const REMOVE_NOTIFICATION = 'removeNotification';

// Notifications types

export const NOTIFICATION_SUCCESS = 'success';
export const NOTIFICATION_WARNING = 'warning';
export const NOTIFICATION_INFO = 'info';
export const NOTIFICATION_DANGER = 'danger';
export const NOTIFICATION_PRIMARY = 'primary';

// Constants for urls
export const API_URL_PREFIX = 'api';

export const urlBase = process.env.NODE_ENV === 'development' ? 'http://localhost:8080':
    'https://backend.anavys.ch';


// export const urlBase = 'https://anavys-backend.propulsion-learn.ch/';
export const LOGIN_URL = '/login';
export const REDIRECT_URL = '/admin/account';




