// import axios from 'axios'
import {API} from './constants';
import {axiosClient} from '../drf/actions'
import {errors} from "./actions/errors";


export default ({dispatch, getState}) => next => action => {

    if (action.type === API) {
        const options = {
            method: action.method || 'get',
        };

        if (action.data) {
            options.data = JSON.stringify(action.data);
        }
        const actionCreator = action.success;

        axiosClient(`${action.url}`, options).then(result => {
            const data = result.data;
            dispatch(actionCreator(data));
        }).catch(err => {
            dispatch(errors(err))
        })

    } else {
        return next(action);
    }


}