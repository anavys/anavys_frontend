import { 
    ADD_NOTIFICATION, 
    REMOVE_NOTIFICATION,
 } from '../constants'


export const notificationInitialState = {
    type: 'success',
    message: "",
    errorResponse: {}
};

export const notifications = (state = notificationInitialState, action) => {
    switch (action.type) {
        case ADD_NOTIFICATION:
            const newState = {...state, ...action.payload};
            return newState;
        case REMOVE_NOTIFICATION:
             const newStateRm = {...state, type: 'success', message: "", errorResponse: {}};
            return newStateRm;
        default:
            return state;
    }
};

