import {
  ADD_TOKENS,
  ADD_TOKENS_TO_STATE, REMOVE_CURRENT_USER, REMOVE_TOKENS, SET_CURRENT_USER
} from '../constants'

export const currentUser = (state = {}, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return Object.assign({}, state, action.payload.user);
    case REMOVE_CURRENT_USER:
      return {};
    default:
      return state;
  }
};

export const tokens = (state = null, action) => {
  switch (action.type) {
    case ADD_TOKENS:
      const { access, refresh } = action.payload;
      access && localStorage.setItem('access', access);
      refresh && localStorage.setItem('refresh', refresh);
      return { access, refresh };
    case ADD_TOKENS_TO_STATE:
      // const { access, refresh } = action.payload;
      return {
        access: action.payload.access,
        refresh: action.payload.refresh
      };

    case REMOVE_TOKENS:
      return null;

    default:
      return state;
  }
};