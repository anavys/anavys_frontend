import { combineReducers } from 'redux';


import { notifications } from './notifications'
import {drfReducer, mapBoxPopupLocation, mapIsCentered} from '../../drf/reducers';
import { viewport } from "../../drf/reducers";
import { tokens, currentUser } from './auth'


export default combineReducers({
  currentUser,
  tokens,
  notifications,
  drfReducer,
  viewport,
  mapIsCentered,
  mapBoxPopupLocation
});

