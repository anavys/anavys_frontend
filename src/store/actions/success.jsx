import {
    ADD_NOTIFICATION,
    REMOVE_NOTIFICATION,
    NOTIFICATION_SUCCESS
} from "../constants";
import {notificationInitialState} from "../reducers/notifications";


export const success = status => (dispatch, getState) => {
    if(!status) return;
    const notification = notificationInitialState;

    notification.type = NOTIFICATION_SUCCESS;
    notification.response = {};

    dispatch({type: REMOVE_NOTIFICATION});

    switch (status) {
        case 200:
            notification.message = 'Okay!';
            break;
        case 201:
            notification.message = 'Successfully created.';
            break;
        case 202:
            notification.message = 'Accepted. All ok.';
            break;
        case 204:
            notification.message = 'Successfully deleted.';
            break;
        default:
            // notification.message = `Add message for status ${status}`;
            break;
    }


    dispatch(({type: ADD_NOTIFICATION, payload: notification}));

    setTimeout(() => {
        dispatch(({type: REMOVE_NOTIFICATION}))
    }, 5000)
};