import {
    API,
    REMOVE_CURRENT_USER,
    ADD_TOKENS,
    ADD_TOKENS_TO_STATE,
    // SEND_REFRESH_TOKEN,
    SET_CURRENT_USER, LOGIN_URL, REMOVE_TOKENS,
} from '../constants';
import {axiosClient, customFetcher} from '../../drf/actions';
import axios from 'axios';
import {errors} from "./errors";

const setCurrentUser = (user) => ({
    type: SET_CURRENT_USER,
    payload: {user}
});

const removeCurrentUser = () => ({
    type: REMOVE_CURRENT_USER,
});

const addTokensToStorage = (data) => ({
    type: ADD_TOKENS,
    payload: data
});


export const addTokensToState = (data) => ({
    type: ADD_TOKENS_TO_STATE,
    payload: data
});


export const fetchCurrentUser = () => async dispatch => {
    const config = {
        method: 'GET',
        id: 'current',
    };
    try {
        const res = await axiosClient(`/api/users/1/`, config);
        if (res.status === 200) {
            dispatch(setCurrentUser(res.data));

        } else {
            // Clear Localstorage and redirect
            localStorage.clear();
        }
    } catch (error) {
        dispatch(errors(error))
    }

};

export const refreshToken = (retryFetch) => async (dispatch) => {
    const refreshToken = localStorage.getItem('refresh')
    const config = {
        method: 'POST',
        data: {refresh: refreshToken},
    };
    try {
        const res = await axiosClient(`/api/auth/token/refresh/`, config);
        if (res.status === 200) {
            // TODO: res.data undefined when api not there.
            dispatch(addTokensToStorage(res.data));
            // Re-fetch also the current user
            dispatch(fetchCurrentUser());

            axios.defaults.headers.common = {'Authorization': `Bearer ${res.data.access}`}
            if(retryFetch) {
                dispatch(customFetcher(retryFetch));
            }

        } else {
            // Clear Localstorage and redirect
            localStorage.clear();
        }
        return res;
    } catch (error) {
        // Handle Network Error
        if (error.message === 'Network Error') {
            dispatch(errors(error))
        } else {
            if (error.response && error.response.data.detail === 'Token is invalid or expired') {
                dispatch(logout())
            }
        }
    }
};

export const login = (data) => async (dispatch) => {
    const config = {
        method: 'POST',
        data: JSON.stringify(data),
    };
    try {
        const res = await axiosClient(`/api/auth/token/`, config);
        const {access, refresh, user} = res.data;
        dispatch(addTokensToStorage(res.data));
        dispatch(setCurrentUser(user));
        axios.defaults.headers.common = {'Authorization': `Bearer ${access}`}
    } catch (error) {
    }
};

export const logout = (event, history) => (dispatch) => {
    // Todo make redirect work
    event && event.preventDefault();

    dispatch({type: REMOVE_TOKENS});
    dispatch(removeCurrentUser());
    localStorage.clear();
    history && history.push('/login');
};

const registerUser = (user) => (dispatch) => {
    dispatch(setCurrentUser(user));
    localStorage.setItem('access', user.token);
};

export const register = (data) => ({
    type: API,
    url: '/api/users',
    method: 'POST',
    success: registerUser,
    data,
});



