import {
    ADD_NOTIFICATION,
    NOTIFICATION_WARNING,
    NOTIFICATION_DANGER,
    REMOVE_NOTIFICATION
} from "../constants";
import {notificationInitialState} from "../reducers/notifications";


// export const notificationInitialState = {
//     type: 'success',
//     message: null,
//     errorResponse: {}
// };

export const errors = errorResponse => (dispatch) => {
    if(!errorResponse) return;
    const notification = notificationInitialState;
    // Handle API not available
    if (errorResponse.message === 'Network Error') {
        notification.type = NOTIFICATION_DANGER;
        notification.message = 'Network Error! The API is not available.'
    } else {
        // Handle if API responded
        const status = errorResponse.response ? errorResponse.response.status: null;

        notification.type = NOTIFICATION_WARNING;
        notification.errorResponse = errorResponse;
        dispatch({type: REMOVE_NOTIFICATION});


        switch (status) {
            case 500:
                notification.message = 'There was an error on the server. Please contact the administrator.';
                notification.type = NOTIFICATION_DANGER;
                break;
            case 400:
                notification.message = 'Malformatted request.';
                break;
            case 401:
                notification.message = 'Failed to authorize. Please contact the site administrator.';
                break;
            case 403:
                notification.message = 'Method not allowed.';
                break;
            case 404:
                notification.message = 'Ressource not found.';
                break;
            default:
                // notification.message = errorResponse.message;
                console.log(errorResponse);
                break;

        }
    }

    notification.message && dispatch(({type: ADD_NOTIFICATION, payload: notification}));

    setTimeout(() => {
        dispatch(({type: REMOVE_NOTIFICATION}))
    }, 2000)
};