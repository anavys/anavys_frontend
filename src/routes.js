// @material-ui/icons
import Person from "@material-ui/icons/Person";
import LocationOn from "@material-ui/icons/LocationOn";
import Vibration from '@material-ui/icons/Vibration';
import LocalPostOffice from '@material-ui/icons/LocalPostOffice';
import Edit from '@material-ui/icons/Edit';
import SettingsRemote from '@material-ui/icons/SettingsRemote';
import EventNoteSharp from '@material-ui/icons/EventNoteSharp';

// import Notifications from "@material-ui/icons/Notifications";
// import Unarchive from "@material-ui/icons/Unarchive";
// import Language from "@material-ui/icons/Language";
// core components/views for Admin layout
// import Empty from "views/Empty/Empty.jsx"
import Projects from "views/Projects/Projects.jsx"
import Account from "views/Account/Account.jsx";
import Typography from "views/Typography/Typography.jsx";
import Icons from "views/Icons/Icons.jsx";
import FieldSites from "./views/FieldSites/FieldSites";
// import DashboardPage from "views/Dashboard/Dashboard.jsx";
// import UserProfile from "views/UserProfile/UserProfile.jsx";
// import TableList from "views/TableList/TableList.jsx";
// import Maps from "views/Maps/Maps.jsx";
// import NotificationsPage from "views/Notifications/Notifications.jsx";
import FieldDevices from "./views/FieldDevices/FieldDevices";
import Addresses from "./views/Addresses/Addresses";
import DeviceModels from "./views/DeviceModels/DeviceModels";
import FieldEventsCatCustom from "./views/FieldEventsCatCustom/FieldEventsCatCustom";
import FieldEvents from "./views/FieldEvents/FieldEvents";
import Measurements from "./views/Measurements/Measurements";
// core components/views for RTL layout
// import RTLPage from "views/RTLPage/RTLPage.jsx";

const dashboardRoutes = [
    {
        path: "/account",
        name: "Account",
        rtlName: "Account",
        icon: Person,
        component: Account,
        layout: "/admin"
    },
    {
        path: "/addresses",
        name: "Addresses",
        rtlName: "Addresses",
        icon: LocalPostOffice,
        component: Addresses,
        layout: "/admin"
    },
    {
        path: "/projects",
        name: "Projects",
        rtlName: "Projects",
        icon: 'content_paste',
        component: Projects,
        layout: "/admin"
    },
    {
        path: "/field-sites",
        name: "Field Sites",
        rtlName: "Field Sites",
        icon: LocationOn,
        component: FieldSites,
        layout: "/admin"
    },
    {
        path: "/field-devices",
        name: "Field Devices",
        rtlName: "Field Devices",
        icon: SettingsRemote,
        component: FieldDevices,
        layout: "/admin"
    },
    {
        path: "/device-models",
        name: "Device Models",
        rtlName: "Device Models",
        icon: Vibration,
        component: DeviceModels,
        layout: "/admin"
    },
    {
        path: "/field-events",
        name: "Field Events",
        rtlName: "Field Events",
        icon: EventNoteSharp,
        component: FieldEvents,
        layout: "/admin"
    },
    {
        path: "/measurements",
        name: "Measurements",
        rtlName: "Measurements",
        icon: Edit,
        component: Measurements,
        layout: "/admin"
    },
    {
        path: "/category-custom",
        name: "My Event Categories",
        rtlName: "My Event Categories",
        icon: Edit,
        component: FieldEventsCatCustom,
        layout: "/admin"
    },
    // {
    //   path: "/dashboard",
    //   name: "Router Name",
    //   rtlName: "Reversed Empty",
    //   icon: Dashboard,
    //   component: Empty,
    //   layout: "/admin"
    // },
    // {
    //   path: "/dash",
    //   name: "Dashboard",
    //   rtlName: "Dashbord",
    //   icon: Dashboard,
    //   component: DashboardPage,
    //   layout: "/admin"
    // },
    // {
    //   path: "/user",
    //   name: "User Profile Original",
    //   rtlName: "Reversed User Profile",
    //   icon: Person,
    //   component: UserProfile,
    //   layout: "/admin"
    // },
    // {
    //   path: "/table",
    //   name: "Table List",
    //   rtlName: "Reversed Tables",
    //   icon: "content_paste",
    //   component: TableList,
    //   layout: "/admin"
    // },
    // {
    //     path: "/typography",
    //     name: "Typography",
    //     rtlName: "Reversed Typography",
    //     icon: LibraryBooks,
    //     component: Typography,
    //     layout: "/admin"
    // },
    // {
    //     path: "/icons",
    //     name: "Icons",
    //     rtlName: "Revesed Icons",
    //     icon: BubbleChart,
    //     component: Icons,
    //     layout: "/admin"
    // },
    // {
    //   path: "/maps",
    //   name: "Maps",
    //   rtlName: "Reversed Maps",
    //   icon: LocationOn,
    //   component: Maps,
    //   layout: "/admin"
    // },
    // {
    //   path: "/notifications",
    //   name: "Notifications",
    //   rtlName: "Reversed Notifications",
    //   icon: Notifications,
    //   component: NotificationsPage,
    //   layout: "/admin"
    // }
];

export default dashboardRoutes;
