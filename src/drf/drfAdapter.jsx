// import axios_instance from '../axios.js';
import axios from 'axios'
import querystring from 'querystring';

import {
    GET_LIST,
    GET_ONE,
    CREATE,
    UPDATE,
    UPDATE_MANY,
    DELETE,
    DELETE_MANY,
    GET_MANY,
    GET_MANY_REFERENCE,
    DRF_ENDPOINTS
} from './constants';

import { urlBase } from '../store/constants'
import {isURL} from "./utils";

const API_URL = urlBase + '/api';




/**
 * Maps queries to the default format of Django REST Framework
 */


/**
 * @param {Object} response HTTP response from axios
 * @param {String} type React-admin request type, e.g. 'GET_LIST'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params Request parameters. Depends on the request type
 * @returns {Object} Data of the response (response.data) or a list of data with the many types
 */
const convertHttpResponse = (resolved_resp, type, resource, params) => {
    const { headers, data } = resolved_resp;     // is resolved  in axios
    switch (type) {
        case GET_LIST:
        case GET_MANY_REFERENCE:
            if ('count' in data) {
                return { data: data.results, total: data.count, status: resolved_resp.status }
            } else if (headers.has('content-range')) {
                return {
                    data: data,
                    total: parseInt(
                        headers
                            .get('content-range')
                            .split('/')
                            .pop(),
                        10
                    ),
                    status: resolved_resp.status
                };
            } else if ('detail' in data && data.detail === 'Invalid page.') {
                return { data: [], total: 0, status: resolved_resp.status }
            } else {
                throw new Error(
                    'The total number of results is unknown. The DRF data provider ' +
                    'expects responses for lists of resources to contain this ' +
                    'information to build the pagination. If you\'re not using the ' +
                    'default PageNumberPagination class, please include this ' +
                    'information using the Content-Range header OR a "count" key ' +
                    'inside the response.'
                );
            }
        case CREATE:
            return { data: { ...params.data, id: data.id }, status: resolved_resp.status };
        case DELETE:
            return { data: params.previousData, status: resolved_resp.status };
        default:
            return { data: data, status: resolved_resp.status };
    }
};

const drfProvider = (apiUrl = API_URL, axiosClient) => {
    /**
* @param {String} type type of api request , e.g. 'GET_LIST'
* @param {String} resource Name of the resource to fetch, e.g. 'posts'
* @param {Object} params Request parameters. Depends on the request type
* @returns {Object} { url, options } The HTTP request parameters, options that are compatible with ,
*/
    const convertDataRequestToHttp = (type, resource, params) => {
        // These parameters will be overwritten depending on case,
        // Thats why nothing is returned.
        let url = "";
        let options = {
            method: 'GET'
        };
        // Check if ressource is in DRF_ENDPOINTS
        // if(DRF_ENDPOINTS.indexOf(resource) === -1){
        //     throw Error(`The endpoint ${resource} is not defined in the constans file under DRF_ENDPOINTS`)
        // }
        switch (type) {
            case CREATE:
                url = `${apiUrl}/${resource}/`;
                options.method = 'POST';
                options.data = JSON.stringify(params.data);
                break;
            case GET_ONE:
                url = isURL(params.id) ? params.id: `${apiUrl}/${resource}/${params.id}/`;
                break;
            case GET_LIST: {
                const { page, perPage } = params.pagination;
                const { field, order } = params.sort;
                const { filter } = params;

                // const query = {
                //     page,...this.props
                //     page_size: perPage,
                //     ordering: `${order === 'ASC' ? '' : '-'}${field}`,
                //     ...filter
                // };
                const query = { ...filter };
                if (page) { query['page'] = page }

                if (perPage) { query['page_size'] = perPage }
                if (field && order) { query['ordering'] = `${order === 'ASC' ? '' : '-'}${field}` }
                url = `${apiUrl}/${resource}/?${querystring.stringify(query)}`;
                break;
            }
            case GET_MANY_REFERENCE: {
                const { page, perPage } = params.pagination;
                const { field, order } = params.sort;
                const { filter, target, id } = params;
                const query = {
                    page,
                    page_size: perPage,
                    ordering: `${order === 'ASC' ? '' : '-'}${field}`,
                    ...filter,
                    [target]: id
                };
                url = `${apiUrl}/${resource}/?${querystring.stringify(query)}`;
                break;
            }
            case UPDATE:
                url = isURL(params.id) ? params.id :`${apiUrl}/${resource}/${params.id}/`;
                options.method = 'PATCH';
                options.data = JSON.stringify(params.data);
                break;
            case DELETE:
                url = isURL(params.id) ? params.id: `${apiUrl}/${resource}/${params.id}/`;
                options.method = 'DELETE';
                break;
            default:
                throw new Error(`Unsupported Data Provider request type ${type}`);
        }

        return { url, options };
    };

    /**
     * @param {String} type React-admin request type, e.g. 'GET_LIST'
     * @param {string} resource Name of the resource to fetch, e.g. 'posts'
     * @param {Object} params Request parameters. Depends on the request type
     * @returns {Promise} the Promise for a data response
     */
    return (type, resource, params) => {
        /**
         * Split GET_MANY, UPDATE_MANY and DELETE_MANY requests into multiple promises,
         * since they're not supported by default.
         */
        switch (type) {
            case GET_MANY:
                return axios.all(
                    params.ids.map(id =>
                        axiosClient(isURL(id) ? id: `${apiUrl}/${resource}/${id}/`, {
                            method: 'get'
                        })
                    )
                ).then(axios.spread((...fetches) => ({
                    data: fetches.map(response => response.data),
                })));
            case UPDATE_MANY:
                return axios.all(
                    params.ids.map(id =>
                        axiosClient(isURL(id) ? id: `${apiUrl}/${resource}/${id}`, {
                            method: 'put',
                            data: JSON.stringify(params.data),
                        })
                    )
                ).then(axios.spread((...fetches) => ({
                    data: fetches.map(response => response.data),
                })));
            case DELETE_MANY:
                return axios.all(
                    params.ids.map(id =>
                        axiosClient(isURL(id) ? id: `${apiUrl}/${resource}/${id}`, {
                            method: 'delete',
                        })
                    )
                ).then(axios.spread((...fetches) => ({
                    data: fetches.map(response => response.data),
                })));
            default:
                break;
        }
        const { url, options } = convertDataRequestToHttp(type, resource, params);
        // debugger;
        return axiosClient(url, options)
            .then(response => {
                return convertHttpResponse(response, type, resource, params)
            });
    }
};

export default drfProvider;
