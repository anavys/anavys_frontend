import {
    GET_LIST,
    GET_ONE,
    UPDATE,
    DELETE,
    UPDATE_VIEWPORT,
    ADD_MAPBOX_LOCATION,
    GET_MANY,
    drfMapInitialState,
    ADD_DRF_MAP_CENTER,
    SET_MAP_CENTERED,
    REMOVE_MAP_CENTERED
} from './constants'

const drfInitialState = {getOne: {}, getMany: {}, getList: {}};

export const drfReducer = (state = drfInitialState, action) => {
    const {type, resource, data} = action;
    // if(!type) throw `No typer was given, ${resource}, ${data}`
    let newState = {...state};
    switch (type + resource) {
        case GET_LIST + resource:
            newState.getList[resource] = data;
            return newState;
        case UPDATE + resource:
        case GET_ONE + resource:
            newState.getOne[resource] = data;
            return newState;
        case DELETE + resource:
            newState.getOne[resource] = null;
            return newState;
        case GET_MANY + resource:
            newState.getMany[resource] = data;
            return newState;
        default:
            return newState;
    }
};


export const viewport = (state = {}, action) => {
    // Todo: how to get rid of undefined key in redux state containing drfMapInitialState?
    const {type, resource, viewport, longitude, latitude} = action;
    switch (type + resource) {
        case UPDATE_VIEWPORT + resource:
            return {...state, [resource]: viewport};
        case ADD_DRF_MAP_CENTER + resource:
            const newState = {...state};
            newState[resource].longitude = longitude;
            newState[resource].latitude = latitude;
            return newState;
        default:
            return {...state, [resource]: drfMapInitialState};
    }
};

export const mapIsCentered = (state = {}, action) => {
    const {type, resource} = action;
    switch (type + resource) {
        case SET_MAP_CENTERED + resource:
            return {...state, [resource]: true};
        case REMOVE_MAP_CENTERED:
            return {...state, [resource]: false};
        default:
            return state;
    }
};

export const mapBoxPopupLocation = (state = {}, action) => {
    const {type, resource, longitude, latitude} = action;
    switch (type + resource) {
        case ADD_MAPBOX_LOCATION + resource:
            return {[resource]: {longitude, latitude}};
        default:
            return state;
    }

};