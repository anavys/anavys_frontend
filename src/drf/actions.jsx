import drfProvider from './drfAdapter'
import axios from 'axios';
import {urlBase, API_URL_PREFIX} from '../store/constants';
import {errors} from "../store/actions/errors";
import {success} from "../store/actions/success";
import {refreshToken} from "../store/actions/currentUser";
import {getForeignKeyIdsFromList, getForeignKeyIdsFromOne, onlyUnique} from "./utils";
import {
    ADD_DRF_MAP_CENTER,
    ADD_MAPBOX_LOCATION, FIELD_FOREIGN_KEY,
    GET_LIST,
    GET_MANY,
    GET_ONE, initialState, INPUT_FOREIGN_KEY, REMOVE_MAP_CENTERED, SET_MAP_CENTERED,
    UPDATE_VIEWPORT
} from "./constants";


const API_URL = `${urlBase}/${API_URL_PREFIX}`;

const axios_instance = axios.create({
    baseURL: urlBase
});

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.put['Content-Type'] = 'application/json';
axios.defaults.headers.patch['Content-Type'] = 'application/json';

export const axiosClient = (url, options = {}) => {

    if (!options.method) {
        options['method'] = 'GET'
    }

    options['Content-Type'] = 'application/json';

    const token = localStorage.getItem('access');
    if (token) {
        axios.defaults.headers.common = {'Authorization': `Bearer ${token}`}

        if (!url === '/api/auth/token/refresh/') {
            options['headers'] = {
                // TODO: add just the ressources needed, not all
                'Access-Control-Allow-Origin': '*',
            }
        }
    }

    return axios_instance(url, options);

};

// export const getForeignKeyIdsFromList = (config, fetchResult) => {
//     const fkFields = config.filter(conf => conf.type === FIELD_FOREIGN_KEY || conf.type === INPUT_FOREIGN_KEY);
//     const uniqueResources = fkFields.map(conf => conf.field).filter(onlyUnique);
//
//     uniqueResources.forEach(conf => {
//         let resource = conf.relatesTo;
//         let ids = fetchResult.data.map(obj => obj[conf.field]);
//     });
// };

const getFkFields = (config) => config.filter(conf => conf.type === FIELD_FOREIGN_KEY || conf.type === INPUT_FOREIGN_KEY);
;

export const fetchFkResources = ({prevType, prevFetchResult, config}) => dispatch => {
    const fkFields = getFkFields(config);
    if (fkFields.length === 0) return null;
    const uniqueResources = fkFields.map(conf => conf.relatesTo).filter(onlyUnique);
    // console.log('fetching fk ressources for prev Type: ', prevType);
    switch (prevType) {
        case GET_LIST:
            uniqueResources.forEach(resource => {
                let ids = [];
                let fields = config.filter(conf => conf.relatesTo === resource);
                fields.forEach(conf => {
                    prevFetchResult.data.forEach(obj => {
                        ids.push(obj[conf.field])
                    })
                });
                const uniqueIds = ids.filter(onlyUnique);
                dispatch(customFetcher({
                    type: GET_MANY,
                    resource,
                    params: {...initialState, ids: uniqueIds},
                    config,
                    fkFetch: true
                }))

            });
            break;
        case GET_ONE:
            uniqueResources.forEach(resource => {
                dispatch(customFetcher({
                    type: GET_LIST,
                    resource,
                    params: initialState.params,
                    config,
                    fkFetch: true
                }))
            });
        default:
            return null;
    }
};


export const customFetcher = ({
                                  type,
                                  resource,
                                  params,
                                  url = API_URL,
                                  history,
                                  redirectUrl,
                                  config,
                                  fkFetch
                              }) => (dispatch) => {

    const _provider = drfProvider(url, axiosClient)(type, resource, params);
    _provider.then(result => {
        // Dispatch data to state with drfReducer
        dispatch({type, resource, data: result});
        // handle foreign keys, does nothing if no FK in config
        if (!fkFetch) {
            dispatch(fetchFkResources({
                prevType: type,
                prevFetchResult: result,
                history,
                redirectUrl,
                config,
                fkFetch: true
            }))
        }
        dispatch(success(result.status));
        // redirect on success
        redirectUrl && history && history.push(redirectUrl);


    }).catch(err => {
        // handle 401 unauthorized when access token is invalid
        if (err.status === 401) {
            dispatch(refreshToken({type, resource, params, history, redirectUrl})
            )
        }
        dispatch(errors(err))
    });
};


export const prefetchFksCreateView = (resource, config) => dispatch => {
    const fkFields = getFkFields(config);
    if(!fkFields) return;
    const uniqueResources = fkFields.map(conf => conf.relatesTo).filter(onlyUnique);
    if(!uniqueResources) return;
    const params = initialState.params;

    uniqueResources.forEach(resource => {
        dispatch(customFetcher({type: GET_LIST, resource, params, config, fkFetch:true}))
    })


};


export const updateViewport = (resource, viewport) => (dispatch) => {
    dispatch(({type: UPDATE_VIEWPORT, resource, viewport}));
};

export const addMapBoxLocation = (longitude, latitude, resource) => dispatch => {
    dispatch({type: ADD_MAPBOX_LOCATION, longitude, latitude, resource})
};

export const addDrfMapCenter = (resource, longitude, latitude) => (dispatch, getState) => {
    const state = getState();
    if (!state.mapIsCentered[resource]) {
        dispatch({type: ADD_DRF_MAP_CENTER, longitude, latitude, resource})
    }
};

export const setDrfMapToCentered = (resource) => (dispatch) => {
    dispatch({type: SET_MAP_CENTERED, resource})
};

export const removeMapCentered = resource => (dispatch) => {
    dispatch({type: REMOVE_MAP_CENTERED, resource})
};