import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import Radio from '@material-ui/core/Radio';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';

const styles = {
    root: {
        color: green[600],
        '&$checked': {
            color: green[500],
        },
    },
    checked: {},
};

const RadioButton = props => {
    const { classes, color, conf, value, disabled } = props;
    return (
            <Radio
                checked={value}
                name={'radio-button-' + conf.field }
                disabled={disabled ? disabled: false}
                // classes={{
                //     root: classes.root,
                //     checked: classes.checked,
                // }}
                color={color ? color: 'primary'}
            />
    );
};

RadioButton.propTypes = {
    classes: PropTypes.object.isRequired,
    color: PropTypes.string,
    conf: PropTypes.object.isRequired,
    // value: PropTypes.boolean.isRequired
};

export default withStyles(styles)(RadioButton);