import React from "react";
import PropTypes from "prop-types";
import {Link} from 'react-router-dom';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// core components
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Edit from "@material-ui/icons/Edit";
import Close from "@material-ui/icons/Close";
// import Checkbox from "@material-ui/core/Checkbox";
// import Check from "@material-ui/core/SvgIcon/SvgIcon";
// styles
import tasksStyle from "assets/jss/material-dashboard-react/components/tasksStyle.jsx";
import tableStyle from "assets/jss/material-dashboard-react/components/tableStyle.jsx";
import {getField} from "./fieldSwitcher";

function CustomTable({...props}) {

    const {classes,
        tableHead,
        tableData,
        tableHeaderColor,
        config,
        detailName,
        match,
        url} = props;

    return (
        <div className={classes.tableResponsive}>
            <Table className={classes.table}>
                {tableHead !== undefined ? (
                    <TableHead className={classes[tableHeaderColor + " " + "TableHeader"]}>
                        <TableRow>
                            {/*<TableCell*/}
                            {/*   className={classes.tableCell + " " + classes.tableHeadCell}*/}
                            {/* >*/}
                            {/*   **/}
                            {/* </TableCell>*/}
                            {tableHead.map((fieldName, key) => {
                                return (
                                    <TableCell
                                        className={classes.TableCell + " " + classes.tableHeadCell}
                                        key={key}
                                    >
                                        {fieldName}
                                    </TableCell>

                                );
                            })}
                            <TableCell
                                className={classes.enhancedTableCell + " " + classes.tableHeadCell}
                            >
                                ' </TableCell>
                        </TableRow>
                    </TableHead>
                ) : null}
                <TableBody>
                    {tableData.map((item, key) => {
                        const editToolTipTitle = `Edit ${detailName}`
                        return (
                            <TableRow key={key}>
                                {/*<TableCell >*/}
                                {/*    <Checkbox*/}
                                {/*        // checked={this.state.checked.indexOf(value) !== -1}*/}
                                {/*        tabIndex={-1}*/}
                                {/*        // onClick={this.handleToggle(value)}*/}
                                {/*        checkedIcon={<Check className={classes.checkedIcon} />}*/}
                                {/*        icon={<Check className={classes.uncheckedIcon} />}*/}
                                {/*        classes={{*/}
                                {/*            checked: classes.checked,*/}
                                {/*            root: classes.root*/}
                                {/*        }}*/}
                                {/*    />*/}
                                {/*</TableCell>*/}
                                {config.map((conf, key) => {
                                    return (
                                        <TableCell key={key} config={config} className={classes.enhancedTableCell}>

                                            {getField({
                                                conf,
                                                value: item[conf.field],
                                                match,
                                                url
                                            })}

                                        </TableCell>
                                    );
                                })}
                                <TableCell align='right'>
                                    <Tooltip
                                        id="tooltip-top"
                                        title={editToolTipTitle}
                                        placement="top"
                                        classes={{tooltip: classes.tooltip}}
                                    >
                                        <IconButton
                                            aria-label="Edit"
                                            className={classes.tableActionButton}
                                        >
                                            <Link to={`${match.path}/${item.id}/edit`}>
                                                <Edit
                                                    className={
                                                        classes.tableActionButtonIcon + " " + classes.edit
                                                    }
                                                />
                                            </Link>

                                        </IconButton>
                                    </Tooltip>

                                    <Tooltip
                                        id="tooltip-top-start"
                                        title="Remove"
                                        placement="top"
                                        classes={{tooltip: classes.tooltip}}
                                    >
                                        <IconButton
                                            aria-label="Close"
                                            className={classes.tableActionButton}
                                            onClick={() => props.handleDelete(item.id)}
                                        >
                                            <Close
                                                className={
                                                    classes.tableActionButtonIcon + " " + classes.close
                                                }
                                            />
                                        </IconButton>
                                    </Tooltip>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </div>
    );
}

CustomTable.defaultProps = {
    tableHeaderColor: "gray"
};

CustomTable.propTypes = {
    classes: PropTypes.object.isRequired,
    config: PropTypes.array.isRequired,
    tableHeaderColor: PropTypes.oneOf([
        "warning",
        "primary",
        "danger",
        "success",
        "info",
        "rose",
        "gray"
    ]),
    tableHead: PropTypes.arrayOf(PropTypes.string),
    tableData: PropTypes.arrayOf(PropTypes.object),
    detailName: PropTypes.string.isRequired,
    match: PropTypes.object.isRequired,
    url: PropTypes.string.isRequired
};

export default withStyles({...tasksStyle, ...tableStyle})(CustomTable);
