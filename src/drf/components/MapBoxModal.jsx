import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import MapBox from "../../mapbox/components/MapBox/MapBox";
import {Marker} from "react-map-gl";
import AddLocation from '@material-ui/icons/AddLocation';
import Button from "../../components/CustomButtons/Button";
import {Dialog} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import CustomInput from "../../components/CustomInput/CustomInput";
import {drfMapInitialState} from "../constants";


const styles = theme => ({

    paper: {
        // width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        // padding: theme.spacing.unit * 4,
        // outline: 'none',
        display: 'flex',
        alignContent: 'center',
        justifyItems: 'center'
    }
});


const addLocationStyle = {
    cursor: 'pointer',
    minWidth: '25px',
    minHeight: '25px',
    color: 'red'
};

class MapBoxModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            open: false,
            viewport: drfMapInitialState,
            location: this.props.initLocation || {
                longitude: null,
                latitude: null
            }
        };

    }

    handleOpen = () => {
        this.setState({
            open: true,
            viewport: {...this.state.viewport,
                longitude:this.props.initLocation.longitude,
                latitude: this.props.initLocation.latitude
            }
        });
    };

    handleClose = () => {
        this.setState({
            open: false,
            viewport: {...this.state.viewport,
                longitude:this.props.initLocation.longitude,
                latitude: this.props.initLocation.latitude
            }
        });
    };

    handleSave = () => {
        const location = {
            longitude: this.state.viewport.longitude,
            latitude: this.state.viewport.latitude
        };
        this.setState({
            location,
            open: false,

        });
        // this.props.addMapBoxLocation(
        //     this.state.viewport.longitude,
        //     this.state.viewport.latitude,
        //     this.props.resource);
        const fakeEvent = {target: {value: location}};
        this.props.onChange(fakeEvent)
    };

    render() {
        const {classes} = this.props;
        const coordDisplay = `${this.props.initLocation.longitude} / ${this.props.initLocation.latitude}`;
        return (
            <div>
                <CustomInput

                    labelText='Location'
                    formControlProps={{
                        fullWidth: true
                    }}
                    inputProps={{
                        // onChange: onChange(conf.field),
                        value: coordDisplay,
                        type: 'text',
                        onClick: this.handleOpen
                    }}
                />
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"

                >
                    <DialogTitle id="alert-dialog-title">{"Choose a location"}</DialogTitle>
                    <DialogContent>
                        <MapBox
                            width={400}
                            height={400}
                            viewport={this.state.viewport}
                            onViewportChange={(viewport) => this.setState({viewport})}
                        >
                            <Marker
                                longitude={this.state.viewport.longitude}
                                latitude={this.state.viewport.latitude}
                            >
                                <AddLocation style={addLocationStyle}/>
                            </Marker>
                        </MapBox>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleSave} color="primary" autoFocus size='sm'>
                            Save
                        </Button>
                        <Button onClick={this.handleClose} color="primary" size='sm'>
                            Go Back
                        </Button>
                    </DialogActions>
                    <div className={classes.paper}>

                    </div>
                </Dialog>
            </div>
        );
    }
}

MapBoxModal.propTypes = {
    classes: PropTypes.object.isRequired,
    resource: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    initLocation: PropTypes.object
};

// const mapStateProps = (state, props) => {
//     const location = state.mapBoxPopupLocation[props.resource];
//     if (location) {
//         return {location}
//     }
//     return null;
// };
//
// const mapDispatchToProps = dispatch => ({
//     addMapBoxLocation: (longitude, latitude, resource) => dispatch(addMapBoxLocation(
//         longitude, latitude, resource))
// });
//
// export default connect(mapStateProps, mapDispatchToProps)(withStyles(styles)(MapBoxModal));
export default withStyles(styles)(MapBoxModal);