import React from "react";
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {INPUT_TEXT, INPUT_DATE, GET_ONE} from '../constants';
import {getField} from "./fieldSwitcher";
import {Link} from 'react-router-dom';

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";


// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
// import CustomInput from "components/CustomInput/CustomInput.jsx";
import CardFooter from "../../components/Card/CardFooter";
import Button from "../../components/CustomButtons/Button";
import {customFetcher} from "../actions";
import {initialState} from '../constants'


const styles = {
    // cardCategoryWhite: {
    //   "&,& a,& a:hover,& a:focus": {
    //     color: "rgba(255,255,255,.62)",
    //     margin: "0",
    //     fontSize: "14px",
    //     marginTop: "0",
    //     marginBottom: "0"
    //   },
    //   "& a,& a:hover,& a:focus": {
    //     color: "#FFFFFF"
    //   }
    // },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    }
};

class DrfDetail extends React.Component {

    constructor(props) {
        super(props)
    }


    componentDidMount() {
        const params = initialState;
        params.id = this.props.match.params.id;
        this.props.fetchResource(this.props.url, params, this.props.config);
    }

    render() {
        const {classes, detail, config, detailName, match, url, location} = this.props;
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12} lg={6}>
                    <Card>
                        <CardHeader color="primary">
                            <h4 className={classes.cardTitleWhite}>{'Details of ' + detailName}</h4>
                            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                {/*<p className={classes.cardCategoryWhite}>Last Modified: </p>*/}
                            </div>
                        </CardHeader>
                        <CardBody>
                            {config.map((conf, key) => {
                                return (
                                    <GridContainer key={key}>
                                        <GridItem xs={12} sm={12} md={12}>

                                            {getField(
                                                {
                                                    conf,
                                                    value: detail ? detail[conf.field] : ""
                                                }
                                            )}

                                        </GridItem>
                                    </GridContainer>
                                )
                            })}
                        </CardBody>
                        <CardFooter>
                            <Link to={match.path.replace('/:id/show', '')}>
                                <Button color="primary">Back</Button>
                            </Link>
                        </CardFooter>

                    </Card>

                </GridItem>
            </GridContainer>
        );
    }

}


DrfDetail.propTypes = {
    url: PropTypes.string.isRequired,
    config: PropTypes.array.isRequired,
    listName: PropTypes.string.isRequired,
    detailName: PropTypes.string.isRequired,
    headerColor: PropTypes.oneOf([
        "primary",
        "info",
        "success",
        "warning",
        "danger",
        "rose",
        "white",
        "transparent"
    ]),
    buttonColor: PropTypes.oneOf([
        "primary",
        "info",
        "success",
        "warning",
        "danger",
        "rose",
        "white",
        "transparent"
    ]),

};

const mapStateToProps = (state, props) => {
    const data = state.drfReducer.getOne;
    if (data && data[props.url]) {
        return {detail: data[props.url].data};
    }
    ;
    return {detail: null};
};

const mapDispatchToProps = dispatch => ({
    fetchResource: (resource, params, config) => dispatch(customFetcher({type: GET_ONE, resource, params, config})),
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DrfDetail));
