import React from 'react';
import {
    INPUT_TEXT,
    INPUT_DATE,
    INPUT_BOOL,
    INPUT_TEXT_AREA,
    FIELD_DATE,
    FIELD_TEXT_AREA,
    FIELD_BOOL, FIELD_TEXT, INPUT_SELECT, INPUT_LOCATION, ON_OVERVIEW_MAP, INPUT_FOREIGN_KEY, FIELD_FOREIGN_KEY,
} from '../constants'

// core components
// import TextField from '@material-ui/core/TextField';
// import DatePicker from "./DatePicker";
import RadioButton from "./RadioButton";
// import DatePicker from "./DatePicker";
import CustomInput from '../../components/CustomInput/CustomInput'
import Checkbox from '@material-ui/core/Checkbox';
import moment from 'moment';
import MapBoxModal from "./MapBoxModal";
import FieldForeignKey from "./FieldForeignKey";
import InputForeignKey from "./InputForeignKey";

const datePickerDateToDjangoDate = datePickerDate => onChange => {

};

export const getField = ({conf, value, onChange, url, match}) => {
    switch (conf.type) {

        case FIELD_TEXT:
            return value;

        case FIELD_TEXT_AREA:
            return value;

        case INPUT_DATE:
            // const datePickerDate = moment(value).format('YYYY-MM-DD');
            // 2017-05-24
            return (
                <CustomInput
                    labelText={conf.fieldName}
                    id={conf.field + '-inputDate'}
                    key={conf.field + '-inputDate'}
                    formControlProps={{
                        fullWidth: true
                    }}
                    inputProps={{
                        onChange: onChange(conf.field),
                        value: value,
                        type: 'date',
                    }}
                />
            );

        case INPUT_SELECT:
            return (
                <CustomInput
                    labelText={conf.fieldName}
                    id={conf.field + '-inputSelect'}
                    key={conf.field + '-inputSelect'}
                    formControlProps={{
                        fullWidth: true
                    }}
                    inputProps={{
                        onChange: onChange(conf.field),
                        value: value,
                    }}
                    select
                    options={conf.options}
                />
            );

        case FIELD_DATE:
            // will render field mapped from redux state, time conversion is in
            // mapStateToProps, hence just return the blank value
            return value;

        case INPUT_BOOL:
            return (
                <div>
                    <Checkbox
                        key={conf.field + '-inputBool'}
                        checked={value}
                        color='secondary'
                        onClick={onChange(conf.field)}

                    />
                    {conf.fieldName}
                </div>

            );

        case FIELD_BOOL:
            return (
                <Checkbox
                    checked={value}
                    color='secondary'
                    id={conf.field + '-fieldBool'}
                    key={conf.field + '-fieldBool'}
                    // conf={conf}
                    // if onChange function is not defined, then nothing happens on click

                />
            );

        case INPUT_TEXT:
            return (
                <CustomInput
                    labelText={conf.fieldName}
                    id={conf.field + '-inputText'}
                    key={conf.field + '-inputText'}
                    formControlProps={{
                        fullWidth: true
                    }}
                    inputProps={{
                        onChange: onChange ? onChange(conf.field) : () => console.log('no onChange'),
                        value: value,
                    }}
                />);

        case INPUT_TEXT_AREA:
            return (
                <CustomInput
                    labelText={conf.fieldName}
                    id={conf.field + '-inputTextArea'}
                    key={conf.field + '-inputTextArea'}
                    formControlProps={{
                        fullWidth: true
                    }}
                    inputProps={{
                        onChange: onChange ? onChange(conf.field) : () => console.log('no onChange'),
                        value: value,
                        multiline: true
                    }}
                />);

        case INPUT_LOCATION:
            return (
                <MapBoxModal
                    resource={url}
                    onChange={onChange(conf.field)}
                    initLocation={value}
                />
            );

        case ON_OVERVIEW_MAP:
            // Do not show this as a single field, but as a map with multiple object
            return null;
        case FIELD_FOREIGN_KEY:
            return (
                <FieldForeignKey
                    conf={conf}
                    value={value}
                    match={match}
                    url={url}
                />
            );
        case INPUT_FOREIGN_KEY:
            return (
                <InputForeignKey
                    onChange={onChange}
                    value={value}
                    conf={conf}
                    match={match}
                    url={url}
                />
            );

        default:
            return value;

    }
}