import React from 'react';

import classNames from "classnames";
import {PropTypes} from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
// @material-ui/icons
// import Clear from "@material-ui/icons/Clear";
// import Check from "@material-ui/icons/Check";
// core components
import customInputStyle from "assets/jss/material-dashboard-react/components/customInputStyle.jsx";
import TextField from "@material-ui/core/TextField";

const styles = {
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: '5px',
        marginRight: '5px',
        width: 200,
    },
};


const DatePicker = (props) => {
    const {
        error,
        success,
        classes,
        conf,
        onChange,
        value
    } = props;

     const labelClasses = classNames({
    [" " + classes.labelRootError]: error,
    [" " + classes.labelRootSuccess]: success && !error
  });

    return (
        <TextField
            id={conf.field}
            label={conf.fieldName}
            type="date"
            value={value}
            onChange={onChange}
            className={classes.textField}
            InputLabelProps={{
                shrink: true,
                // className: {classes.labelRoot + labelClasses}
            }}
        />
    );
};

DatePicker.propTypes = {
    classes: PropTypes.object.isRequired,
    // formControlProps: PropTypes.object,
    // error: PropTypes.bool,
    // success: PropTypes.bool,
    conf: PropTypes.object.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};


// const DatePicker = props => {
//     const {conf, value, onChange, classes} = props;
//     return (
//         <form className={classes.container} noValidate>
//             <TextField
//                 id={conf.field}
//                 label={conf.fieldName}
//                 type="date"
//                 value={value}
//                 onChange={onChange}
//                 className={classes.textField}
//                 InputLabelProps={{
//                     shrink: true,
//                 }}
//             />
//         </form>
//     );
// }


export default withStyles({...styles, ...customInputStyle})(DatePicker);