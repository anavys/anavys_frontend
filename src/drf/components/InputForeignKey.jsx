import CustomInput from "../../components/CustomInput/CustomInput";
import React from "react";
import {connect} from "react-redux";
import {PropTypes} from "prop-types";
import {ID_FIELD_NAME} from "../constants";

const InputForeignKey = props => {
    const {conf, value, onChange} = props;

    let options = [{'label': 'None', value: null}];

    // getList will be fetch for all the related fk resources first
    if (props.getList && props.getList.data) {
        // item for display in edit view
        // const item = props.getList.data.filter(obj => obj[ID_FIELD_NAME] === value)[0];
        // if(item)  val = item[conf.relatedDisplayField];

        options = props.getList.data.map(obj => {
            return {label: obj[conf.relatedDisplayField], value: obj[ID_FIELD_NAME]}
        });
    }

    return (
        <CustomInput
            labelText={conf.fieldName}
            id={conf.field + '-inputSelect'}
            key={conf.field + '-inputSelect'}
            formControlProps={{
                fullWidth: true
            }}
            inputProps={{
                onChange: onChange(conf.field),
                value: value,
            }}
            select
            options={options}
        />
    );
};

const mapStateToProps = (state, props) => ({
    getList: state.drfReducer.getList[props.conf.relatesTo],
});

InputForeignKey.propTypes = {
    conf: PropTypes.object.isRequired,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    onChange: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    url: PropTypes.string.isRequired

};

export default connect(mapStateToProps)(InputForeignKey)
