import React from "react";
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import {
    initialState,
    drfMapInitialState,
    DELETE,
    GET_ONE,
    UPDATE,
    INPUT_BOOL,
    FIELD_DATE,
    INPUT_DATE,
    CREATE,
    INPUT_LOCATION,

} from '../constants'
import {customFetcher, prefetchFksCreateView} from '../actions';
import moment from 'moment';

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import CardFooter from "../../components/Card/CardFooter";
import Button from "../../components/CustomButtons/Button"

// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import {getField} from './fieldSwitcher'
import {Link} from "react-router-dom";

const styles = {
    // cardCategoryWhite: {
    //   "&,& a,& a:hover,& a:focus": {
    //     color: "rgba(255,255,255,.62)",
    //     margin: "0",
    //     fontSize: "14px",
    //     marginTop: "0",
    //     marginBottom: "0"
    //   },
    //   "& a,& a:hover,& a:focus": {
    //     color: "#FFFFFF"
    //   }
    // },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    }
};

class DrfCreate extends React.Component {
    constructor(props) {
        super(props);
        const inputFields = {};
        this.props.config.forEach(conf => inputFields[conf.field] = "");
        this.props.config.forEach(conf => conf.type === INPUT_LOCATION ?
            inputFields[conf.field] = drfMapInitialState : null);


        this.dateFields = this.props.config.filter(
            conf => conf.type === INPUT_DATE);

        this.booleanFields = this.props.config.filter(
            conf => conf.type === INPUT_BOOL
        );

        if (this.dateFields.length !== 0) {
            this.dateFields.forEach(conf => {
                inputFields[conf.field] = moment().format('YYYY-MM-DD');
            })
        }

        if (this.booleanFields.length !== 0) {
            this.booleanFields.forEach(conf => {
                inputFields[conf.field] = false;
            })
        }

        this.state = {
            inputFields: inputFields,
            initialData: true,
            ...initialState,
        };
    }

    componentDidMount() {
        this.props.prefetchFksCreateView(this.props.url, this.props.config);
    }


    handleChange = value => e => {

        const testBool = this.props.config.filter(conf => conf.field === value)[0].type === INPUT_BOOL;

        let val = testBool ? e.target.checked : e.target.value;

        const newInputFields = {...this.state.inputFields};

        newInputFields[value] = val;
        this.setState({
            inputFields: newInputFields
        })

    };

    handleSubmit = e => {
        e.preventDefault();
        const params = {...this.state.params};
        const data = {...this.state.inputFields};

        if (this.dateFields.length !== 0) {
            this.dateFields.forEach(conf => {
                data[conf.field] = moment(data[conf.field]).format();
            })
        }
        params.data = data;

        const redirectUrl = this.props.match.path.replace('/create', '');
        this.props.createOne(this.props.url, params, this.props.history, redirectUrl, this.props.config);
    };


    render() {
        const {classes, config, detailName, match, headerColor, buttonColor} = this.props;
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={10} lg={8}>
                    <Card>
                        <CardHeader color={headerColor}>
                            <h4 className={classes.cardTitleWhite}>Create {detailName}</h4>
                            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                {/*<p className={classes.cardCategoryWhite}>Last Modified: </p>*/}
                            </div>
                        </CardHeader>
                        <form onSubmit={this.handleSubmit}>
                            <CardBody>
                                {config.map((conf, key) => {
                                    return (
                                        <GridContainer key={key}>
                                            <GridItem xs={12} sm={12} md={12}>

                                                {getField(
                                                    {
                                                        conf,
                                                        value: this.state.inputFields[conf.field],
                                                        onChange: this.handleChange,
                                                        url: this.props.url,
                                                        match
                                                    }
                                                )}

                                            </GridItem>
                                        </GridContainer>
                                    )
                                })}
                            </CardBody>
                            <CardFooter>
                                <Button color={buttonColor} type='submit'>Create</Button>
                                <Link to={match.path.replace('/create', '')}>
                                    <Button color={buttonColor}>Back</Button>
                                </Link>

                            </CardFooter>
                        </form>
                    </Card>

                </GridItem>
            </GridContainer>
        );
    }
}

DrfCreate.propTypes = {
    url: PropTypes.string.isRequired,
    config: PropTypes.array.isRequired,
    headerColor: PropTypes.oneOf([
        "primary",
        "info",
        "success",
        "warning",
        "danger",
        "rose",
        "white",
        "transparent"
    ]),
    buttonColor: PropTypes.oneOf([
        "primary",
        "info",
        "success",
        "warning",
        "danger",
        "rose",
        "white",
        "transparent"
    ]),
};


const mapDispatchToProps = dispatch => ({
    createOne: (resource, params, history, redirectUrl, config) => dispatch(customFetcher({
        type: CREATE,
        resource,
        params,
        history,
        redirectUrl,
        config
    })),
    prefetchFksCreateView: (resource, config) => dispatch(prefetchFksCreateView(resource, config))
});

export default connect(null, mapDispatchToProps)(withStyles(styles)(DrfCreate));
