import React from "react";
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import {initialState, GET_ONE, UPDATE, INPUT_BOOL, INPUT_DATE, INPUT_LOCATION, drfMapInitialState} from '../constants'
import {customFetcher} from '../actions';
import moment from 'moment'

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import CardFooter from "../../components/Card/CardFooter";
import Button from "../../components/CustomButtons/Button"

// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import {getField} from './fieldSwitcher'
import {Link} from "react-router-dom";

const styles = {
    // cardCategoryWhite: {
    //   "&,& a,& a:hover,& a:focus": {
    //     color: "rgba(255,255,255,.62)",
    //     margin: "0",
    //     fontSize: "14px",
    //     marginTop: "0",
    //     marginBottom: "0"
    //   },
    //   "& a,& a:hover,& a:focus": {
    //     color: "#FFFFFF"
    //   }
    // },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    }
};

class DrfEdit extends React.Component {
    constructor(props) {
        super(props);
        const inputFields = {};
        this.props.config.forEach(conf => inputFields[conf.field] = "");
        this.props.config.forEach(conf => conf.type === INPUT_LOCATION ?
            inputFields[conf.field] = drfMapInitialState : null);
        this.state = {
            inputFields: inputFields,
            initialData: true,
            ...initialState,
        };
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        const newParams = {...this.state.params, id: id};
        this.props.fetchResource(this.props.url, newParams, this.props.config);
        this.setState({
            params: newParams
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.initialData && this.props.getOne) {
            const newInputFields = {};
            this.props.config.forEach(conf => {
                newInputFields[conf.field] = this.props.getOne[conf.field] || "";
            });
            this.setState({
                inputFields: newInputFields,
                initialData: false,
            })
        }
    }

    handleChange = value => e => {
        const testBool = this.props.config.filter(conf => conf.field === value)[0].type === INPUT_BOOL;

        let val = testBool ? e.target.checked : e.target.value;

        const newInputFields = {...this.state.inputFields};

        newInputFields[value] = val;
        this.setState({
            inputFields: newInputFields
        })
    };


    handleSubmit = e => {
        e.preventDefault();

        const dateFields = this.props.config.filter(
            conf => conf.type === INPUT_DATE);

        const params = {...this.state.params};
        const data = {...this.state.inputFields};

        if (dateFields.length !== 0) {
            dateFields.forEach(conf => {
                data[conf.field] = moment(data[conf.field]).format();
            })
        }


        params.data = data;
        const redirectUrl = this.props.match.path.replace('/:id/edit', '');
        this.props.updateOne(this.props.url, params, this.props.history, redirectUrl, this.props.config);
    };


    render() {
        const {classes, url, config, detailName, match, headerColor, buttonColor} = this.props;
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={10} lg={8}>
                    <Card>
                        <CardHeader color={headerColor}>
                            <h4 className={classes.cardTitleWhite}>Edit {detailName}</h4>
                            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                {/*<p className={classes.cardCategoryWhite}>Last Modified: </p>*/}
                            </div>
                        </CardHeader>
                        <form onSubmit={event => this.handleSubmit(event)}>
                            <CardBody>
                                {config.map((conf, key) => {
                                    return (
                                        <GridContainer key={`${key}-${conf.field}`}>
                                            <GridItem xs={12} sm={12} md={12}>

                                                {getField(
                                                    {conf,
                                                    value: this.state.inputFields[conf.field],
                                                    onChange: this.handleChange,
                                                    url,
                                                    match
                                                    }

                                                )}

                                            </GridItem>
                                        </GridContainer>
                                    )
                                })}
                            </CardBody>
                            <CardFooter>
                                <Button
                                    color={buttonColor}
                                    type='submit'
                                >
                                    Update</Button>
                                <Link to={match.path.replace('/:id/edit', '')}>
                                    <Button color={buttonColor}>Back</Button>
                                </Link>

                            </CardFooter>
                        </form>
                    </Card>

                </GridItem>
            </GridContainer>
        );
    }
}

DrfEdit.propTypes = {
    url: PropTypes.string.isRequired,
    config: PropTypes.array.isRequired,
    headerColor: PropTypes.oneOf([
        "primary",
        "info",
        "success",
        "warning",
        "danger",
        "rose",
        "white",
        "transparent"
    ]),
    buttonColor: PropTypes.oneOf([
        "primary",
        "info",
        "success",
        "warning",
        "danger",
        "rose",
        "white",
        "transparent"
    ]),
};


const mapStateToProps = (state, props) => {
    const getOne = {...state.drfReducer.getOne};
    const {config, url} = props;
    const dateFields = config.filter(
        conf => conf.type === INPUT_DATE);

    const newState = {getOne: null, getMany: null};

    if (getOne && getOne[url]) {
        newState.getOne = {...getOne[url].data};

        // Convert dates to target field compatible value
        if (dateFields.length !== 0) {
            dateFields.forEach(conf => {
                newState.getOne[conf.field] = moment(
                    newState.getOne[conf.field]).format('YYYY-MM-DD');
            })
        }

    }
    const getMany = {...state.drfReducer.getMany};
    if (getMany) {
        newState.getMany = getMany
    }

    return newState
};

const mapDispatchToProps = dispatch => ({
    fetchResource: (resource, params, config) => dispatch(customFetcher({type: GET_ONE, resource, params, config})),
    updateOne: (resource, params, history, redirectUrl, config) => dispatch(customFetcher({
        type: UPDATE,
        resource,
        params,
        history,
        redirectUrl,
        config
    }))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DrfEdit));
