import React from 'react';
import MapBox from "../../mapbox/components/MapBox/MapBox";
import Paper from "@material-ui/core/Paper";
import {PropTypes} from "prop-types";
import {Marker, SVGOverlay} from "react-map-gl";
import {Explore, MyLocation} from "@material-ui/icons";
import {SVG_DOT, ICON_MARKER, ON_OVERVIEW_MAP, drfMapInitialState} from "../constants";
import {primaryColor} from "../../assets/jss/material-dashboard-react";
import {connect} from "react-redux";
import {addDrfMapCenter, setDrfMapToCentered, updateViewport} from "../actions";
import {getCenterFromCoords} from "../utils";
import LocationOn from '@material-ui/icons/LocationOn'

const drawShape = (longitude, latitude, color, radius) => ({project}) => {
    const [cx, cy] = project([longitude, latitude]);
    return (<circle cx={cx} cy={cy} r={radius} fill={color}/>);
};

const markers = [
    {
        longitude: 8.536720,
        latitude: 47.37747
    },
    {
        longitude: 8.536715,
        latitude: 47.37740
    },
];

const getMarker = ({
                       markerType = SVG_DOT,
                       color = primaryColor[0],
                       radius = 6,
                       icon,
                       latitude,
                       longitude,
                       key


                   }) => {
    // const _icon = icon ? icon : <MyLocation color='primary'/>;
    const _icon = icon ? icon : <LocationOn color='secondary'/>;
    switch (markerType) {
        case SVG_DOT:
            return (
                <SVGOverlay
                    key={key}
                    redraw={drawShape(longitude, latitude, color, radius)}
                />
            );

        case ICON_MARKER:
            return (
                <Marker
                    key={key}
                    longitude={longitude}
                    latitude={latitude}
                >
                    {_icon}
                </Marker>
            );

        default:
            return null;
    }
};

const DrfMap = props => {
    const {config, resource, list} = props;

    const locationFields = config.filter(conf => conf.type === ON_OVERVIEW_MAP);
    const markers = [];
    const coordList = [];

    if (list && locationFields.length !== 0) {
        locationFields.forEach((conf, i) => {
                props.list.forEach((obj, j) => {
                    markers.push(
                        getMarker({
                            key: `${conf.field}-${j}${i}`,
                            longitude: obj[conf.field]['longitude'],
                            latitude: obj[conf.field]['latitude'],
                            ...conf
                        })
                    );
                    coordList.push([obj[conf.field]['longitude'],obj[conf.field]['latitude']]
                    )
                })
            }
        );
        const centerCoords = getCenterFromCoords(coordList);
        // console.log(centerCoords)
        if(centerCoords && !props.mapIsCentered){
             props.addDrfMapCenter(resource, centerCoords[0], centerCoords[1]);
             props.setDrfMapToCentered(resource);
        }


    }


    return (
        markers.length !== 0 ?
            <Paper style={{marginBottom: '20px'}}>
                <MapBox
                    width={props.width}
                    height={props.height}
                    viewport={props.viewport}
                    onViewportChange={viewport => props.updateViewport(resource, viewport)}
                >
                    {markers}
                </MapBox>
            </Paper> : null
    )
};

DrfMap.propTypes = {
    resource: PropTypes.string.isRequired,
    config: PropTypes.arrayOf(PropTypes.object.isRequired),
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

};


const mapStateToProps = (state, props) => {

    const newState = {};
    const viewport = state.viewport[props.resource];

    if (viewport) {
        newState.viewport = viewport;
    } else {
        newState.viewport = drfMapInitialState
    }

    const lists = state.drfReducer.getList;
    if (lists && lists[props.resource] && lists[props.resource]['data']) {
        newState.list = lists[props.resource]['data']
    } else {
        newState.list = null
    }

    if(state.mapIsCentered[props.resource] !== undefined){
        newState.mapIsCentered = state.mapIsCentered[props.resource]
    }

    return newState;

};


const mapDispatchToProps = dispatch => ({
    updateViewport: (resource, viewport) => dispatch(updateViewport(resource, viewport)),
    setDrfMapToCentered: (resource) => dispatch(setDrfMapToCentered(resource)),
    addDrfMapCenter: (resource, longitude, latitude) => dispatch(addDrfMapCenter(resource, longitude, latitude))

});

export default connect(mapStateToProps, mapDispatchToProps)(DrfMap);