import React from "react";
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import {GET_LIST, initialState, DELETE, DEFAULT_PAGE_SIZE, FIELD_DATE} from '../constants'
import {customFetcher, removeMapCentered} from '../actions'

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CustomTable from "./Table";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import ArrowBack from '@material-ui/icons/ArrowBackIos';
import ArrowForward from '@material-ui/icons/ArrowForwardIos';
import moment from "moment";

import {roseColor} from "../../assets/jss/material-dashboard-react";
import {Link} from "react-router-dom";
import Button from "../../components/CustomButtons/Button";
import DrfMap from "./DrfMap";


const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    navIcons: {
        margin: '15px 15px'
    },
    icon: {
        color: roseColor[0],
        margin: '5px',
        '&:hover': {
            color: roseColor[2],
        },
    },
    flexRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
};


class DrfList extends React.Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    componentDidMount() {
        this.props.fetchResource(this.props.url, this.state.params, this.props.config);
    //    Set mapbox was centered to false again
        this.props.removeMapCentered(this.props.url);
    }

    handleDelete(id) {
        const params = {...this.state.params};
        params.id = id;
        // debugger
        // customFetcher((DELETE, this.props.url, {...params, id:id}))

        this.props.deleteOne(this.props.url, params, this.props.config);
        this.props.fetchResource(this.props.url, params, this.props.config);
    }

    handleChangePage(e, increment) {
        e.preventDefault();
        const total = this.props.getList.total;
        const newParams = this.state.params;
        if (newParams.pagination.page === 1 && increment === -1) return;
        if (total < newParams.pagination.page * DEFAULT_PAGE_SIZE) return;
        newParams.pagination.page += increment;
        this.props.fetchResource(this.props.url, newParams, this.props.config);
        this.setState(
            {params: newParams}
        );
    }


    render() {
        const {
            classes,
            getList,
            listName,
            config,
            url,
            detailName,
            match,
            headerColor,
            buttonColor
        } = this.props;

        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12} lg={12}>
                    <DrfMap
                        resource={url}
                        config={config}
                    />
                    <Card plain>
                        <CardHeader plain color={headerColor}>
                            <div className={classes.flexRow}>
                                <h4 className={classes.cardTitleWhite}>
                                    {listName}
                                </h4>
                                <Link to={`${match.path}/create`}>
                                    <Button
                                        color={buttonColor}
                                        size='sm'
                                    >
                                        Add {listName}
                                    </Button>
                                </Link>

                            </div>
                            {/*  <p className={classes.cardCategoryWhite}>*/}
                            {/*    Click on the Items to Show Detail View*/}
                            {/*</p>*/}
                        </CardHeader>
                        <CardBody>
                            <div>
                                <p> {getList ? `Total: ${getList.total}` : "No data"}</p>

                            </div>
                            <CustomTable
                                tableHeaderColor="success"
                                tableHead={config.map(conf => (conf.fieldName))}
                                tableData={getList.data}
                                config={config}
                                detailName={detailName}
                                match={match}
                                handleDelete={(id) => this.handleDelete(id)}
                                url={url}

                            >
                            </CustomTable>
                            <CardFooter classes={classes.cardFooter}>
                                <a
                                    href="#"
                                    onClick={(e) => this.handleChangePage(e, -1)}
                                    className={classes.navIcons}
                                >
                                    <ArrowBack/>
                                </a>
                                Page {this.state.params.pagination.page}
                                <a
                                    href="#"
                                    onClick={(e) => this.handleChangePage(e, 1)}
                                    className={classes.navIcons}
                                >
                                    <ArrowForward/>
                                </a>
                            </CardFooter>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        );
    }
}

DrfList.propTypes = {
    url: PropTypes.string.isRequired,
    config: PropTypes.array.isRequired,
    headerColor: PropTypes.oneOf([
        "primary",
        "info",
        "success",
        "warning",
        "danger",
        "rose",
        "white",
        "transparent"
    ]),
    buttonColor: PropTypes.oneOf([
        "primary",
        "info",
        "success",
        "warning",
        "danger",
        "rose",
        "white",
        "transparent"
    ]),
};

const mapStateToProps = (state, props) => {
    const getList = state.drfReducer.getList;
    const getMany = state.drfReducer.getMany;
    const {config, url} = props;

    const newState = {getList:{data: [], total: 0}, getMany:null};

    const dateFields = config.filter(
        conf => conf.type === FIELD_DATE);

    if (getList && getList[url]) {
        newState.getList = {...getList[url]};
        if (dateFields.length !== 0) {
            newState.getList.data.forEach(item => {
                dateFields.forEach(conf => {
                    item[conf.field] = moment(item[conf.field]).format('L');
                });
            })
        }
    }

    if(getMany) {
        newState.getMany = getMany;
    }
    return newState;
};

const mapDispatchToProps = dispatch => ({
    fetchResource: (resource, params, config) => dispatch(customFetcher({type: GET_LIST, resource, params, config})),
    deleteOne: (resource, params, config) => dispatch(customFetcher({type: DELETE, resource, params, config})),
    removeMapCentered: (resource) => dispatch(removeMapCentered(resource))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DrfList));
