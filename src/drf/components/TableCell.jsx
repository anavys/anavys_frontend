import React from "react";
import TableCell from "@material-ui/core/TableCell";


const TableCell = props => {
    return (
        <TableCell><p>{props.children}</p></TableCell>
    )
};

export default TableCell;