import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, withRouter } from "react-router-dom";

const Resource = props => {
    // React Router
    const { match, list, edit, create, detail } = props;
    const List = list;
    const Edit = edit;
    const Create = create;
    const Detail = detail;
    return (
        <Switch>
            <Route exact path={`${match.path}`} render={(props) => <List {...props} />} />
            <Route exact path={`${match.path}/:id/show`} render={(props) => <Detail {...props} />} />
            <Route exact path={match.path + '/create'} render={(props) => <Create {...props} />} />
            <Route exact strict path={match.path + '/:id/edit'} render={(props) => <Edit {...props} />} />
        </Switch>
        )

}

Resource.propTypes = {
        url: PropTypes.string.isRequired,
        list: PropTypes.oneOfType([PropTypes.object.isRequired, PropTypes.func.isRequired]),
        edit: PropTypes.oneOfType([PropTypes.object.isRequired, PropTypes.func.isRequired]),
        detail: PropTypes.oneOfType([PropTypes.object.isRequired, PropTypes.func.isRequired]),
        create: PropTypes.oneOfType([PropTypes.object.isRequired, PropTypes.func.isRequired]),
        listName: PropTypes.string.isRequired,
        detailName: PropTypes.string.isRequired,
    };

export default withRouter(Resource);
