import React from "react";
import {connect} from "react-redux";
import {PropTypes} from "prop-types";
import {ID_FIELD_NAME} from "../constants";

const FieldForeignKey = props => {
    const {conf, value, match, url} = props;
    const resource = conf.relatesTo;
    const d = props.getMany;

    let val, id;
    let linkUrl;
    if(props.getMany && props.getMany.data && props.getMany.data.filter(obj => obj[ID_FIELD_NAME] === value)[0]){
        const item = props.getMany.data.filter(obj => obj[ID_FIELD_NAME] === value)[0];
        val = item[conf.relatedDisplayField];
        id = d.data.filter(obj => obj[ID_FIELD_NAME] === value)[0]['id'];
        linkUrl = `${match.url.replace(url, resource)}/${id}/edit`;
    }
    return (
        <div>
            {val ? <a href={linkUrl}>{val}</a>: value}
        </div>
        );
};

const mapStateToProps = (state, props) => ({
    getMany : state.drfReducer.getMany[props.conf.relatesTo]
});

FieldForeignKey.propTypes = {
    conf: PropTypes.object.isRequired,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    match: PropTypes.object.isRequired,
    url: PropTypes.string.isRequired
};

export default connect(mapStateToProps)(FieldForeignKey)
