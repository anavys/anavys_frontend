import WebMercatorViewport from "react-google-maps";

export const onlyUnique = (value, index, self) => self.indexOf(value) === index;

// usage example:
// let a = ['a', 1, 'a', 2, '1'];
// let unique = a.filter( onlyUnique ); // returns ['a', 1, 2, '1']
// console.log(unique)


export const isURL = str => {
    const pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return pattern.test(str);
};


export const getCenterFromCoords = (coordList, viewport) => {
    if(!coordList || !coordList[0]) return ;
    if(coordList.length === 1) return coordList[0];
    const xS = coordList.map(t => t[0]);
    const yS = coordList.map(t => t[1]);
    const xMin = Math.min(...xS);
    const xMax = Math.max(...xS);
    const yMax = Math.max(...yS);
    const yMin = Math.min(...yS);
    const xCenter = xMin + (xMax - xMin) / 2;
    const yCenter = yMin +(yMax -yMin) / 2;

    // const {longitude, latitude, zoom} = new WebMercatorViewport(viewport)
    //         .fitBounds([xMin,xMax, yMin, yMax], {
    //             padding: {
    //                 top: 82,
    //                 bottom: 30,
    //                 left: 30,
    //                 right: 30
    //             }
    //         });
    // debugger

    return [xCenter, yCenter];
};