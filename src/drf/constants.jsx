// DRF ADAPTER
export const GET_LIST = 'getList';
export const GET_ONE = 'getOne';
export const CREATE = 'createOne';
export const UPDATE = 'updateOne';
export const UPDATE_MANY = 'updateMany';
export const DELETE = 'deleteOne';
export const DELETE_MANY = 'deleteMany';
export const GET_MANY = 'getMany';
export const GET_MANY_REFERENCE = 'getManyRef';
export const ADAPTER_TYPES = [
    GET_LIST, GET_ONE, CREATE, UPDATE, UPDATE_MANY,
    DELETE, DELETE_MANY, GET_MANY, GET_MANY_REFERENCE
];

// Input Field Types
export const INPUT_TEXT = 'inputText';
export const INPUT_SELECT = 'inputSelect';
export const INPUT_TEXT_AREA = 'inputTextMultiLine';
export const INPUT_DATE = 'inputDate';
export const INPUT_BOOL = 'inputCheckbox';
export const INPUT_LOCATION = 'inputLocation';
export const FIELD_DATE = 'fieldDate';
export const FIELD_TEXT = 'fieldText';
export const FIELD_BOOL = 'fieldBool';
export const FIELD_TEXT_AREA = 'fieldTextArea';
export const INPUT_PASSWORD = 'inputPassword';
export const FIELD_DATE_TIME = 'fieldDateTime';
export const FIELD_FOREIGN_KEY = 'fieldFK';
export const INPUT_FOREIGN_KEY = 'inputFK';



// DRF settings
export const DEFAULT_PAGE_SIZE = 10;
export const initialState = {
    params: {
      id: null,
      ids: null,
      data: null,
      pagination: {
        page: 1,
        // perPage: null,
      },
      sort: {
        field: 'id',
        order: 'DESC'
      },
      filter: {},
      target: null,
      previousData: null
    }
  };

export const ID_FIELD_NAME = 'url';

// MAPBOX FIELD COMPONENTS and related

export const SVG_DOT = 'svgDot';
export const ICON_MARKER = 'iconMarker';
export const UPDATE_VIEWPORT = 'updateViewPort';
export const ADD_MAPBOX_LOCATION = 'addMapBoxLocation';
export const ON_OVERVIEW_MAP = 'onOverviewMap';

export const drfMapInitialState = {
    width: 'auto',
    height: 400,
    longitude: 8.536724,
    latitude: 47.377474,
    zoom: 13
};
export const ADD_DRF_MAP_CENTER = 'addDrfMapCenter';
export const SET_MAP_CENTERED = 'setMapCentered';
export const REMOVE_MAP_CENTERED = 'removeMapCentered';