import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import SnackbarContent from "../Snackbar/SnackbarContent";

const hideStyle = {
    display: 'none',
    // visibility: 'hidden'
};

const showStyle = {
    display: 'inline-block',
};

const NotificationPopUp = props => {
    const {notifications} = props;
    const {errorResponse, type, message} = notifications;
    const hide = !notifications.message
        &&
        Object.entries(errorResponse).length === 0
        &&
        errorResponse.constructor === Object;

    return (
        <div style={hide ? hideStyle : showStyle}>
            <SnackbarContent
                message={message}
                close
                color={type}
            />
        </div>
    )
};


NotificationPopUp.propTypes = {
    notifications: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    notifications: state.notifications
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(NotificationPopUp)
