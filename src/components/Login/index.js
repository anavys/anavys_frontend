import { connect } from 'react-redux';
import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { login } from '../../store/actions/currentUser';
import './index.css';
import { withRouter } from 'react-router-dom';
import { REDIRECT_URL } from '../../store/constants'

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
    };
  }
  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.login({ ...this.state });
  }

  componentDidMount() {
    this.checkUser();
  }

  componentDidUpdate() {
    this.checkUser();
  }

  checkUser() {
    if (this.props.tokens) {
      this.props.history.push(REDIRECT_URL);
    }
  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <div>
            <TextField
              autoFocus
              placeholder="Username"
              fullWidth
              required
              onChange={this.handleChange('username')}
              value={this.state.email}
              margin='normal'
            />
          </div>
          <div>
            <TextField
              placeholder="Password"
              fullWidth
              onChange={this.handleChange('password')}
              type="password"
              value={this.state.password}
              margin='normal'
            />
          </div>
          <div className="Login-button">
            <Button
              fullWidth
              label="Login"
              type="submit"
              variant='contained'
              color='secondary'
            >Login</Button>
          </div>
        </form>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  login: (data) => dispatch(login(data))
});

const mapStateToProps = state => ({
  tokens: state.tokens
});


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));

