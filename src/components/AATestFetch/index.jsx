import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import CardBody from "components/Card/CardBody.jsx";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButtons/Button.jsx";

import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { GET_LIST } from "../../drf/constants";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

// const testChoices = {
//   label: ''
// }

export class AATESTFetch extends Component {
  // static propTypes = {
  //   type: PropTypes.string.isRequired,
  //   resource: PropTypes.string.isRequired,
  //   params: PropTypes.object.isRequired
  // }

  constructor(props) {
    super(props)
    this.state = {
      fetchType: GET_LIST,
      resource: 'users',
      params: {}
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.fetchJSON({ ...this.state });
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  // fetchInputProps = {
  //   onChange: {this.handleChange('resource')}
  // }

  render() {
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={6}>
          <form onSubmit={this.handleSubmit}>
            <Card>
              {/* <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Edit Profile</h4>
              <p className={classes.cardCategoryWhite}>Complete your profile</p>
            </CardHeader> */}
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      labelText="Fetch Type"
                      id='fetch-types'
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{ onChange: this.handleChange('fetchType') }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      labelText="Resource"
                      id="resource"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{ onChange: this.handleChange('resource') }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      labelText="Filter"
                      id="filter-input"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{ onChange: this.handleChange('filter') }}
                    />
                  </GridItem>

                </GridContainer>
              </CardBody>
              <CardFooter>
                <Button color="primary" type='submit' >Try Fetch Data</Button>
              </CardFooter>
            </Card>
          </form>
        </GridItem>
      </GridContainer>
    )
  }
}

const mapStateToProps = (state) => ({
  state: state
})

const mapDispatchToProps = {
  // fetchJSON: (type, resource, params) => () => { type, resource, params }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AATESTFetch));
