/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import Login from '../../components/Login';
import Register from '../../components/Register';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';


function TabContainer({ children, dir }) {
    return (
        <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
            {children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        width: 500,
    },
});

class LoginRegisterTabs extends React.Component {
    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    handleChangeIndex = index => {
        if (typeof index === 'number') {
            this.setState({ value: index });
        }
    };

    render() {
        const { classes, theme } = this.props;
        return (
            <div className={classes.root}>
                <AppBar position="static" color="default">
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="fullWidth"
                    >
                        <Tab label="Login" />
                        <Tab label="Register" />
                    </Tabs>
                </AppBar>
                <SwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={this.state.value}
                    onClick={this.handleChangeIndex}
                // onChangeIndex={this.handleChangeIndex}
                >
                    <TabContainer dir={theme.direction}> <Login /></TabContainer>
                    <TabContainer dir={theme.direction}><Register /></TabContainer>
                </SwipeableViews>
            </div>
        );
    }
}

LoginRegisterTabs.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser,
        tokens: state.tokens
    };
}

// export default connect(mapStateToProps)(withRouter(Login));

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(LoginRegisterTabs));