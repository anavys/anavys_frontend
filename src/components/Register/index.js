import { connect } from 'react-redux';
import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { register } from '../../store/actions/currentUser';
import './index.css';

class Register extends Component {

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      username: '',
    };
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.register({
      email: this.state.email,
      username: this.state.username,
      password: this.state.password,
    });
  }

  render() {
    return (
      <div className="Register">
        <form onSubmit={this.handleSubmit}>
          <div>
            <TextField
              autoFocus
              placeholder="Username"
              fullWidth
              margin='normal'
              onChange={this.handleChange('username')}
              value={this.state.username}
            />
          </div>
          <div>
            <TextField
              placeholder="Email"
              fullWidth
              margin='normal'

              onChange={this.handleChange('username')}
              value={this.state.email}
            />
          </div>
          <div>
            <TextField
              placeholder="Password"
              fullWidth
              onChange={this.handleChange('password')}
              margin='normal'
              type="password"
              value={this.state.password}
            />
          </div>
          <div>
            <TextField
              placeholder="Confirm Password"
              fullWidth
              type="password"
              margin='normal'
              onChange={this.handleChange('confirmPassword')}
              value={this.state.confirmPassword}
            />
          </div>
          <div className="Register-button">
            <Button
              fullWidth
              margin='normal'
              label="Register"
              type="submit"
              color='secondary'
              variant='contained'
            >Register</Button>
          </div>
        </form>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  register: (data) => dispatch(register(data))
});

export default connect(null, mapDispatchToProps)(Register);
