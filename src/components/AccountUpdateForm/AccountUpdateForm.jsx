import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
// import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import {customFetcher} from '../../drf/actions'
import {UPDATE} from "../../drf/constants";
import moment from "moment";


const TITLE_OPTIONS = [
    {label: 'Mr.', value: 'MR' },
    {label: 'Ms.', value: 'MS' },
    {label: 'free', value: 'FREE' },
];

class AccountUpdateForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentUser: {
                username: '',
                email: '',
                first_name: '',
                last_name: '',
                account: {
                    title: '',
                    phone: '',
                },
            },
            initialData: true
        };
    }


    handleChange = value => e => {
        const newCurrentUser = {...this.state.currentUser};
        newCurrentUser[value] = e.target.value;
        this.setState({
            currentUser: newCurrentUser
        })
    };

    handleAccountChange = value => e => {
        const newCurrentUser = {...this.state.currentUser};
        newCurrentUser.account[value] = e.target.value;
        this.setState({
                currentUser: newCurrentUser
            }
        )
    };

    handleSubmit = e => {
        e.preventDefault();
        const {username, email, title, first_name, last_name, phone} = this.state.currentUser;

        const params = {
            id: this.state.currentUser.id,
            data: {username, email, title, first_name, last_name, phone}
        };
        this.props.updateUser(params);
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        // debugger
        if (this.state.initialData && this.props.currentUser.id) {
            this.setState({
                currentUser: this.props.currentUser,
                initialData: false,
            })
        }
    }

    render() {
        const {classes} = this.props;
        const account = this.props.currentUser.account;
        const modifiedAt = account ? moment(account.modifiedAt).format('L') : '';
        return (
            <div>
                <GridContainer>
                    <GridItem xs={12} sm={12} md={10} lg={10} xl={10}>
                        <Card>
                            <CardHeader color="primary">
                                <h4 className={classes.cardTitleWhite}>Edit Account</h4>
                                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                    <p className={classes.cardCategoryWhite}>Complete your account profile</p>
                                    <p className={classes.cardCategoryWhite}>{`Last Modified: ${modifiedAt}`}</p>
                                </div>
                            </CardHeader>
                            <form onSubmit={e => this.handleSubmit(e)}>
                                <CardBody>
                                    <GridContainer>
                                        <GridItem xs={12} sm={12} md={6}>
                                            <CustomInput
                                                labelText="Username"
                                                id="username-input"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: this.handleChange('username'),
                                                    value: this.state.currentUser['username']
                                                }}
                                            />
                                        </GridItem>
                                        <GridItem xs={12} sm={12} md={6}>
                                            <CustomInput
                                                labelText="Email address"
                                                id="email-address-input"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: this.handleChange('email'),
                                                    value: this.state.currentUser['email']
                                                }}
                                            />
                                        </GridItem>
                                    </GridContainer>
                                    <GridContainer>
                                        <GridItem xs={12} sm={12} md={2}>
                                            <CustomInput
                                                labelText="Title"
                                                id="title"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: this.handleAccountChange('title'),
                                                    value: this.state.currentUser.account['title']
                                                }}
                                                select
                                                options={TITLE_OPTIONS}
                                            />
                                        </GridItem>
                                        <GridItem xs={12} sm={12} md={5}>
                                            <CustomInput
                                                labelText="First Name"
                                                id="first-name"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: this.handleChange('first_name'),
                                                    value: this.state.currentUser['first_name']
                                                }}
                                            />
                                        </GridItem>
                                        <GridItem xs={12} sm={12} md={5}>
                                            <CustomInput
                                                labelText="Last Name"
                                                id="last-name"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: this.handleChange('last_name'),
                                                    value: this.state.currentUser['last_name']
                                                }}
                                            />
                                        </GridItem>
                                    </GridContainer>
                                    <GridContainer>
                                        {/*<GridItem xs={12} sm={12} md={6}>*/}
                                        {/*    <CustomInput*/}
                                        {/*        labelText="Street"*/}
                                        {/*        id="street"*/}
                                        {/*        formControlProps={{*/}
                                        {/*            fullWidth: true*/}
                                        {/*        }}*/}
                                        {/*        inputProps={{*/}
                                        {/*            onChange: this.handleChange('street'),*/}
                                        {/*            value: this.state.currentUser.address['street']*/}
                                        {/*        }}*/}
                                        {/*    />*/}
                                        {/*</GridItem>*/}
                                        <GridItem xs={12} sm={12} md={6}>
                                            <CustomInput
                                                labelText="Phone"
                                                id="phone"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: this.handleAccountChange('phone'),
                                                    value: this.state.currentUser.account['phone']
                                                }}
                                            />
                                        </GridItem>
                                    </GridContainer>
                                    <GridContainer>
                                        {/*<GridItem xs={12} sm={12} md={6}>*/}
                                        {/*    <CustomInput*/}
                                        {/*        labelText="Street Number"*/}
                                        {/*        id="street-number"*/}
                                        {/*        formControlProps={{*/}
                                        {/*            fullWidth: true*/}
                                        {/*        }}*/}
                                        {/*        inputProps={{*/}
                                        {/*            onChange: this.handleChange('streetNumber'),*/}
                                        {/*            value: this.state.currentUser.address['streetNumber']*/}
                                        {/*        }}*/}
                                        {/*    />*/}
                                        {/*</GridItem>*/}
                                        {/*<GridItem xs={12} sm={12} md={6}>*/}
                                        {/*    <CustomInput*/}
                                        {/*        labelText="Canton"*/}
                                        {/*        id="canton"*/}
                                        {/*        formControlProps={{*/}
                                        {/*            fullWidth: true*/}
                                        {/*        }}*/}
                                        {/*        inputProps={{*/}
                                        {/*            onChange: this.handleChange('canton'),*/}
                                        {/*            value: this.state.currentUser.address['canton']*/}
                                        {/*        }}*/}
                                        {/*    />*/}
                                        {/*</GridItem>*/}
                                    </GridContainer>
                                    <GridContainer>
                                        {/*<GridItem xs={12} sm={12} md={6}>*/}
                                        {/*    <CustomInput*/}
                                        {/*        labelText="Zip Code"*/}
                                        {/*        id="zip"*/}
                                        {/*        formControlProps={{*/}
                                        {/*            fullWidth: true*/}
                                        {/*        }}*/}
                                        {/*        inputProps={{*/}
                                        {/*            onChange: this.handleChange('plz'),*/}
                                        {/*            value: this.state.currentUser.address['plz']*/}
                                        {/*        }}*/}

                                        {/*    />*/}
                                        {/*</GridItem>*/}
                                        {/*<GridItem xs={12} sm={12} md={6}>*/}
                                        {/*    <CustomInput*/}
                                        {/*        labelText="City"*/}
                                        {/*        id="city"*/}
                                        {/*        formControlProps={{*/}
                                        {/*            fullWidth: true*/}
                                        {/*        }}*/}
                                        {/*        inputProps={{*/}
                                        {/*            onChange: this.handleChange('city'),*/}
                                        {/*            value: this.state.currentUser.address['city']*/}
                                        {/*        }}*/}
                                        {/*    />*/}
                                        {/*</GridItem>*/}
                                    </GridContainer>
                                </CardBody>
                                <CardFooter>
                                    <Button color="primary" type='submit'>Update</Button>
                                </CardFooter>
                            </form>
                        </Card>
                    </GridItem>
                </GridContainer>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({

    currentUser: state.currentUser,
});

const mapDispatchToProps = {
    updateUser: (params) => customFetcher({type: UPDATE, resource:'users', params})
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountUpdateForm)


