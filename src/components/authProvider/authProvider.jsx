import React, { Component } from 'react';
import { connect } from 'react-redux';
import { LOGIN_URL } from '../../store/constants'

export default ChildComponent => {

    class ComposedComponent extends Component {

        redirectUser() {
            if (!this.props.tokens) {
                this.props.history.push(LOGIN_URL);
            }
        }

        // The component just got rendered
        componentDidMount() {
            this.redirectUser();
        }

        // The component just got updated
        // componentDidUpdate() {
        //     this.shouldNavigateAway();
        // }

        componentWillUpdate(nextProps) {
            if (!nextProps.tokens) {
                this.redirectUser();
            }
        }

        render() {
            return <ChildComponent {...this.props} />;
        }
    }

    function mapStateToProps(state) {
        return {
            tokens: state.tokens
        };
    }
    return connect(mapStateToProps)(ComposedComponent);
};