import { Route, Redirect } from 'react-router-dom';
import { LOGIN_URL } from '../../store/constants'

const fakeAuth = {
    isAuthenitcated: true
};

const authRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={ props => (
        fakeAuth.isAuthenitcated === true 
        ? <Component {...props}/> 
        : <Redirect to={LOGIN_URL}/>
    )}
     />
)



