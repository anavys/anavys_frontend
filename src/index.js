import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { Provider } from 'react-redux';
// core components
import Admin from "layouts/Admin.jsx";
import RTL from "layouts/RTL.jsx";

import "assets/css/material-dashboard-react.css?v=1.6.0";
import LoginRegisterLayout from './layouts/LoginLayout';
import { store } from './store/index';
// import { MuiThemeProvider} from '@material-ui/core/styles';
// import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import RequireAuth from './components/authProvider/authProvider';
import { LOGIN_URL, REDIRECT_URL } from './store/constants'
import { addTokensToState, refreshToken } from './store/actions/currentUser'

const access = localStorage.getItem('access');
const refresh = localStorage.getItem('refresh');
// if we have the token consider the user to be signed in
if (access && refresh) {
  store.dispatch(addTokensToState({access, refresh}))
  store.dispatch(refreshToken())
} else {
}

const hist = createBrowserHistory();

ReactDOM.render(

  <Provider store={store}>
    {/* <MuiThemeProvider> */}
      <Router history={hist}>
        <Switch>
          <Route path="/admin" component={RequireAuth(Admin)} />
          <Route path="/rtl" component={RTL} />
          <Route path={LOGIN_URL} component={LoginRegisterLayout} />
          <Redirect from='/' to={REDIRECT_URL} />
        </Switch>
      </Router>
    {/* </MuiThemeProvider> */}
  </Provider>,
  document.getElementById("root")
);
