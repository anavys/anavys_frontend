FROM node:11.13.0-alpine

RUN mkdir -p /src
COPY ./package.json /src
COPY ./package-lock.json /src

WORKDIR /src
RUN npm install
COPY . /src
RUN npm run build

FROM nginx:1.15
COPY --from=0 /src/build /var/www/static

