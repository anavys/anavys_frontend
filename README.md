# Anavys Admin Tool Project

The uses a template from CreativeTim. It can be considered as an admin tool, since most of the development
was building CRUD components for a simple API based on django-restframework defaults.  

At first, I started this project with `react-admin` made by marmelab. This was important because the use of
the adapter pattern was inspired by [react-admin's documentation](https://marmelab.com/react-admin/DataProviders.html). 

I then used an [adapter](https://github.com/synaptic-cl/ra-data-drf) for `react-admin` made for 
`django-restframework` and rebuilt the routes, tables, fields, and the redux-store, reducers and actions
in order to only create config objects to plug in a new endpoints.  

## The Resource Component

Following the example of the `FieldDevices` view, a view is exported as a `Resource` component and then registered in
`routes.js` to add it to the sidebar.
The resource component creates the following nested routes:

- `/<url>/`: the list view as table (for mobile work is in progress)
- `/<url>/create`: create a new object
- `/<url>/<:id>/edit`: edit object with id based on url
- `/<url>/<:id>/detail`: detail of the object (work in progress).  

The `Resource` Component takes in the following Components made for every route as props:

- `DrfList`
- `DrfEdit`
- `DrfCreate`
- `DrfDetail`

Every of those 4 CRUD components take their own configuration object that specifies the fields, labels and type of field
to be displayed.

```javascript
const FieldDevices = props => {
    return (
        <Resource
            url='field-devices'
            list={List}
            edit={Edit}
            detail={Detail}
            create={Create}
            listName='Field Devices'
            detailName='Field Device'
            {...props}
        />)
};
```

## Sample View

This sample view illustrates the 4 configuration objects for every CRUD component and the export of the `Resource`
component as a view.  
This illustrates in particular the use of a WGS coordinate field in an overview map as well foreign keys that have
to be re-fetched once the objects were retrieved.

```javascript
import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Resource from '../../drf/components/resource';
import DrfList from '../../drf/components/DrfList';
import DrfEdit from "../../drf/components/DrfEdit";
import DrfDetail from "../../drf/components/DrfDetail";
import DrfCreate from "../../drf/components/DrfCreate";

import {
    FIELD_TEXT,
    INPUT_TEXT,
    FIELD_BOOL,
    INPUT_BOOL,
    ON_OVERVIEW_MAP,
    SVG_DOT,
    INPUT_LOCATION,
    FIELD_FOREIGN_KEY,
    INPUT_FOREIGN_KEY
} from "../../drf/constants";

const RESOURCE = 'field-devices';
const LIST_NAME = 'Field Devices';
const DETAIL_NAME = 'Field Device';

const listConfig = [
    {
        field: 'active',
        fieldName: 'Active',
        type: FIELD_BOOL
    },
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT
    },
    {
        field: 'project',
        fieldName: 'Related Project',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'projects',
        relatedDisplayField: 'name'
    },
    {

        field: 'model',
        fieldName: 'Device Model',
        type: FIELD_FOREIGN_KEY,
        relatesTo: 'device-models',
        relatedDisplayField: 'name',

    },
    {
        field: 'coordinates',
        type: ON_OVERVIEW_MAP,
        markerType: SVG_DOT,
        color: 'orange',
        radius: 6,
    },

];

const editConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: INPUT_TEXT,
    },
    {
        field: 'project',
        fieldName: 'Project',
        type: INPUT_FOREIGN_KEY,
        relatesTo: 'projects',
        relatedDisplayField: 'name',
    },
    {
        field: 'model',
        fieldName: 'Model',
        type: INPUT_FOREIGN_KEY,
        relatesTo: 'device-models',
        relatedDisplayField: 'name',

    },
    {
        field: 'active',
        fieldName: 'Active',
        type: INPUT_BOOL
    },
    {
        field: 'serialNo',
        fieldName: 'Serial Number',
        type: INPUT_TEXT
    },
    {
        field: 'coordinates',
        fieldName: 'Coordinates',
        type: INPUT_LOCATION,
    },
];


const detailConfig = [
    {
        field: 'name',
        fieldName: 'Name',
        type: FIELD_TEXT,
    },

    {
        field: 'Project',
        fieldName: 'project',
        type: FIELD_TEXT,
        options: null
    },
    {
        field: 'Model',
        fieldName: 'model',
        type: FIELD_TEXT,
        options: null
    },
    {
        field: 'active',
        fieldName: 'Active',
        type: FIELD_BOOL
    },
    {
        field: 'serialNo',
        fieldName: 'Serial Number',
        type: FIELD_TEXT
    },
    {
        field: 'coordinates',
        type: ON_OVERVIEW_MAP,
        markerType: SVG_DOT,
        color: 'red',
        radius: 6,
    },

];

const List = props => {

    return (
        <DrfList
            url='field-devices'
            listName={LIST_NAME}
            detailName={DETAIL_NAME}
            headerColor='warning'
            buttonColor='primary'
            config={listConfig}
            {...props} />
    )

};

const Edit = props => {

    return (
        <DrfEdit url={RESOURCE}
                 listName={LIST_NAME}
                 detailName={DETAIL_NAME}
                 headerColor='warning'
                 buttonColor='primary'
                 config={editConfig} {...props} />
    )
};


const Detail = props => {
    return (
        <DrfDetail url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='warning'
                   buttonColor='primary'
                   config={detailConfig} {...props} />
    )
};


const Create = props => {
    return (
        <DrfCreate url={RESOURCE}
                   listName={LIST_NAME}
                   detailName={DETAIL_NAME}
                   headerColor='warning'
                   buttonColor='primary'
                   config={editConfig} {...props} />
    )
};

const FieldDevices = props => {
    return (
        <Resource
            url={RESOURCE}
            list={List}
            edit={Edit}
            detail={Detail}
            create={Create}
            listName={LIST_NAME}
            detailName={DETAIL_NAME}
            {...props}
        />)
};

FieldDevices.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(FieldDevices);

```

## Field Types

The list of field types are listed in [constants.jsx](./src/drf/constants.jsx).

## Future improvements

Refer also to the [issues lsit](https://gitlab.com/anavys/anavys_frontend/issues) for more detailed information.

### Field Types

- add a `DateTime_INPUT` and `DateTime_FIELD`

### Mobile

- make map double-clickable to activate scrolling on-map or make a hide button
- on mobile, views do not take 100% view-width.

## Frontend and Backend

- enable filtering of resources


### Resources

- [How to pass props through Router Component of react-router](https://tylermcginnis.com/react-router-pass-props-to-components/)
- https://tylermcginnis.com/react-router-protected-routes-authentication/
- https://stackoverflow.com/questions/54225512/update-viewport-state-after-fitbounds-is-called
